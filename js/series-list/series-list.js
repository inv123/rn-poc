import React from 'react';
import { View, Text, Button, TextInput, Dimensions  } from 'react-native';

class Main extends React.Component {
  render() {
    return (
        <View style={{
            flex: 1, flexDirection: 'column', alignItems: 'stretch', justifyContent: 'flex-start'
        }}>
            <View style={{height: 50, backgroundColor: 'powderblue'}} />
            <View style={{height: 50, backgroundColor: 'skyblue'}} />
            <View style={{ 
                // flex: 1,
                // flexDirection: 'row',
                // justifyContent: 'space-around',
                margin: 5,
                height: 38
                }}>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <Text style={{ marginTop: 10 }}>Some label:</Text>
                    <TextInput style={{ flex: 1, alignSelf: 'stretch', height: 38 }} />
                </View>
            </View>
            <View style={{ flex: 1, backgroundColor: 'steelblue' }} />
            <View style={{ flex: 2 }}>
                <Text>Some text</Text>
            </View>
            <View style={{ height: 50, backgroundColor: 'powderblue' }} />
        </View>
    );
  }
}

export default Main;