import React, { Component } from "react";
import {
  StyleSheet,
  View,
  PanResponder,
  Animated,
  Text
} from "react-native";

export default class Draggable extends Component {
  constructor() {
    super();
    this.state = {
      pan: new Animated.ValueXY({ x:0, y: 0}),
      value: {
        y: 0
      }
    };
  }

  componentWillMount() {
    // Add a listener for the delta value change
    // this._val = { x:0, y:50 }
    // this.state.pan.addListener((value) => this._val = value);
    this.state.pan.addListener((value) => 
    {
      //alert(value)
      this.setState(Object.assign({}, this.state, { value: { y: value.y * -1 } }))
      console.log(value)
    });

    // Initialize PanResponder with move handling
    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (e, gesture) => true,
      //onMoveShouldSetPanResponder: (e, gesture) => false,
      //   //??? return hijja volt csak?
      //   return false;
      //   if (e.state.pan.y > 100) {
      //     return false;
      //   } else {
      //     return true;
      //   }
      // },
      onPanResponderGrant: () => {
        // Place to initialize stuff -> on click eqviabliten
        //alert(123)
        Animated.spring(this.state.pan, {
          toValue: { x: 0, y: -35 },
        }).start();
      },
      onPanResponderMove: (e, gestureState) => {
        if (this.state.pan.y._value*-1 > 50) {
          return;
        };
        //alert(this.state.pan.y._value)
        return this.state.pan.y._value > 0 ? 
          null :
          Animated.event([ null, { dx: 0, dy: this.state.pan.y }, ])(e, gestureState)
      },
      onPanResponderRelease: (e, gesture) => {
        Animated.spring(this.state.pan, {
          toValue: { x: 0, y: 0 },
          friction: 5
        }).start();
      }
    });
    // adjusting delta value
    //this.state.pan.setValue({ x:0, y:50})
  }

  render() {
    const panStyle = {
      transform: this.state.pan.getTranslateTransform()
    }
    return (
        <Animated.View
          {...this.panResponder.panHandlers}
          style={[panStyle, styles.circle]}
        ><Text>val:{this.state.value.y}</Text></Animated.View>
    );
  }
}

let CIRCLE_RADIUS = 30;
let styles = StyleSheet.create({
  circle: {
    backgroundColor: "skyblue",
    width: CIRCLE_RADIUS * 2,
    height: CIRCLE_RADIUS * 2,
    borderRadius: CIRCLE_RADIUS
  }
});