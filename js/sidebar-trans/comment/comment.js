import React from 'react';
import { View, StyleSheet, Text, Button, Image, FlatList, TouchableWithoutFeedback, TouchableNativeFeedback, TextInput, Dimensions, ScrollView, Animated, Easing } from 'react-native';
import PropTypes from 'prop-types'

import HrLine from 'appteszt/js/components/hr-line'
import Chevron from 'appteszt/js/components/chevron'
import CircleProfile from 'appteszt/js/sidebar-trans/comment/circle-profile'

import { scale, verticalScale } from 'appteszt/js/scaling';

class Comment extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            chiks: [
                {
                    image: require('appteszt/img/sidebar/profile_1.png')
                },
                {
                    image: require('appteszt/img/sidebar/profile_2.png')
                },
                {
                    image: require('appteszt/img/sidebar/profile_3.png')
                },
                {
                    image: require('appteszt/img/sidebar/profile_4.png')
                },
                {
                    image: require('appteszt/img/sidebar/profile_5.png')
                },
                {
                    image: require('appteszt/img/sidebar/profile_2.png')
                },
                {
                    image: require('appteszt/img/sidebar/profile_3.png')
                },
                {
                    image: require('appteszt/img/sidebar/profile_3.png')
                },
                {
                    image: require('appteszt/img/sidebar/profile_3.png')
                },
                {
                    image: require('appteszt/img/sidebar/profile_3.png')
                }
            ]
        };

        this.onDimensionChange = this.onDimensionChange.bind(this);
        //Dimensions.addEventListener('change', this.onDimensionChange);
    }

    onDimensionChange() {
        // To trigger redraw
        this.setState(Object.assign({}, this.state, {}));
    }

    componentWillUnmount() {
        //Dimensions.removeEventListener('change', this.onDimensionChange);
    }

    render() {
        var { height, width } = Dimensions.get('window');

        const numberOfItems = width / 50;

        let itemsIco = this.state.chiks.map((item, i) => {
            //alert(`${i} ${numberOfItems}`)
            return (
                <View>
                    { numberOfItems >= i ? 
                    <CircleProfile {...item} />
                    : null }
                </View>
            );
        });
        itemsIco.push(
            <View style={{
                    backgroundColor: 'red',
                    width: 48,
                    height: 48,
                    borderBottomLeftRadius: 35,
                    borderBottomRightRadius: 35,
                    borderTopLeftRadius: 35,
                    borderTopRightRadius: 35,
                    marginTop: 6,

                    padding: 5,
                    paddingTop: 14,
                    paddingLeft: 8
                }}>
                <Text>+299</Text>
            </View>
        )

        return (
            <View style={{ flex: 1, width: width }}>
                <ScrollView style={{ flex: 1 }}>
                    <View style={{
                            flexDirection: 'row', justifyContent: 'space-between',
                            alignItems: 'center',
                            height: verticalScale(70),
                        }}>
                    
                        <View style={{ width: 10, marginLeft: 20, marginTop: 8,
                        height: 25,
                        transform:[
                            {
                                rotate: '180deg'
                            },
                            {
                                scale: 1.5
                            }
                        ] }}>
                            <TouchableNativeFeedback onPress={() => this.props.changeView('details')}>
                                <View style={{ flex: 1, paddingTop: 5 }}>
                                    <Chevron color='#1f1f1f'></Chevron>
                                </View>
                            </TouchableNativeFeedback>
                        </View>
                    </View>

                    <View style={{ 
                            flex: 1,
                            flexDirection: 'row',
                            justifyContent: 'center',
                            marginRight: 5
                        }}>
                        {itemsIco}
                    </View>
                </ScrollView>
            </View>)
    }
}

//CategorySelector.propTypes = {
//};

export default Comment;
