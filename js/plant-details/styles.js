import { StyleSheet, Dimensions } from 'react-native'
import { scale, moderateScale, verticalScale} from './../scaling';

var { height, width } = Dimensions.get('window');

const styles = {
    header: {
        height: scale(70)
    },
    bottomLine: {
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        height: verticalScale(50),
        backgroundColor: '#9bbaff',
        
        shadowColor: 'blue', shadowOffset: { width: 1, height: 10 }, shadowRadius: 25, shadowOpacity: 1
    },
    bottomText: {
        alignSelf: 'flex-end',
        color: '#e0f2fd',
        fontSize: 16,
        marginRight: scale(12),
        marginTop: verticalScale(8),
        fontFamily: 'cronusRound',
        fontSize: scale(25)
    },
    bottomRightCorner: {
        flex: 1,
        borderTopRightRadius: 10,
        height: verticalScale(80),
        width: 50,
        backgroundColor: '#a3c1ff'
    },
    plantImage: {
        flex: 3,
        // width: scale(160),
        // height: moderateScale(200, 0.5),
        aspectRatio: 0.6,
        transform: [
            { scaleY: 0.75 },
            { scaleX: 0.75 }
        ]
    },
    cultivation: {
        fontWeight: '400',
        color: '#242424',
        marginBottom: 10,
        marginLeft: 25,
        fontSize: scale(20),
    },
    cultivationProgress: {
        fontWeight: '100',
        color: '#242424',
        fontSize: scale(15),
    },
    description: {
        margin: 20,
        lineHeight: 28,
        fontFamily: 'cronusRound',
        fontSize: scale(22),
    },
    descShadow: {
        width: width,
        height: verticalScale(200),
        position: 'absolute',
        marginTop: verticalScale(560),
        transform: [
            { rotate: '180deg'},
        ]
    },
    topLeft: {
        margin: 15,
        marginLeft: 20,
        marginTop: 25
    },
    topRight: {
        margin: 10,
        marginRight: 20,
        marginTop: 15
    },
    middleText: {
        marginTop: 15,
        fontFamily: 'cronusRound',
        fontSize: scale(25),
    },
    descLeft: {
        fontSize: scale(10),
        lineHeight: 16
    },
    descRight: {
        fontSize: scale(14),
        lineHeight: 16
    }
}

export default StyleSheet.create(styles);