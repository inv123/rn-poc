import React from 'react';
import { View, StyleSheet, Text, Button, TouchableWithoutFeedback, TouchableNativeFeedback, TextInput, Dimensions, ScrollView, Animated, Easing } from 'react-native';
import PropTypes from 'prop-types'

class BurgerChevron extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            twoWidth: new Animated.Value(20),

            rotateDeg: new Animated.Value(0),

            topTransX: new Animated.Value(0),
            topTransY: new Animated.Value(0),

            bottomTransX: new Animated.Value(0),
            bottomTransY: new Animated.Value(0)
        };

        this.onDimensionChange = this.onDimensionChange.bind(this);
        //Dimensions.addEventListener('change', this.onDimensionChange);
    }

    onDimensionChange() {
        // To trigger redraw
        this.setState(Object.assign({}, this.state, {}));
    }

    componentWillUnmount() {
        //Dimensions.removeEventListener('change', this.onDimensionChange);
    }

    burger() {
        Animated.parallel([
            Animated.timing(this.state.twoWidth, {
                toValue: 20,
                easing: Easing.out(Easing.exp),
                duration: 500,
            }),
            Animated.timing(this.state.rotateDeg, {
                toValue: 0,
                easing: Easing.out(Easing.exp),
                duration: 500,
            }),

            Animated.timing(this.state.topTransX, {
                toValue: 0,
                easing: Easing.out(Easing.exp),
                duration: 500,
            }),
            Animated.timing(this.state.topTransY, {
                toValue: 0,
                easing: Easing.out(Easing.exp),
                duration: 500,
            }),
            Animated.timing(this.state.bottomTransX, {
                toValue: 0,
                easing: Easing.out(Easing.exp),
                duration: 500,
            }),
            Animated.timing(this.state.bottomTransY, {
                toValue: 0,
                easing: Easing.out(Easing.exp),
                duration: 500,
            })
        ]).start();
    }

    chevron() {
        Animated.parallel([
            Animated.timing(this.state.twoWidth, {
                toValue: 8,
                easing: Easing.out(Easing.exp),
                duration: 500,
            }),
            Animated.timing(this.state.rotateDeg, {
                toValue: 1,
                easing: Easing.out(Easing.exp),
                duration: 500,
            }),

            Animated.timing(this.state.topTransX, {
                toValue: -4,
                easing: Easing.out(Easing.exp),
                duration: 500,
            }),
            Animated.timing(this.state.topTransY, {
                toValue: 1,
                easing: Easing.out(Easing.exp),
                duration: 500,
            }),
            Animated.timing(this.state.bottomTransX, {
                toValue: -2.5,
                easing: Easing.out(Easing.exp),
                duration: 500,
            })
        ]).start();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.headerStage === this.props.headerStage) return;

        let { headerStage } = nextProps;
        if (headerStage == 1 && this.props.headerStage == 0) {
            this.chevron()
        }

        if (headerStage == 0 && this.props.headerStage == 1) {
            this.burger();
        }

        //alert(`${JSON.stringify(nextProps)}, ${JSON.stringify(this.props)}`)
    }

    render() {
        var { height, width } = Dimensions.get('window');

        const spinP = this.state.rotateDeg.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '45deg']
        })

        const spinN = this.state.rotateDeg.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '-45deg']
        })

        return (
            <View style={{ marginLeft: 5, marginTop: 5 }}>
                <Animated.View style={{
                    width: this.state.twoWidth,
                    height: 2,
                    backgroundColor: 'white',
                    transform: [
                        {
                            rotate: spinN
                        },
                        {
                            translateY: this.state.topTransY
                            //translateY: 1
                        },
                        {
                            translateX: this.state.topTransX
                            //translateX: -4
                        }
                    ]
                }}>

                </Animated.View>
                
                <View style={{
                    marginTop: 3,
                    width: 20,
                    height: 2,
                    backgroundColor: 'white'
                }}>

                </View>

                <Animated.View style={{
                    marginTop: 3,
                    width: this.state.twoWidth,
                    height: 2,
                    backgroundColor: 'white',

                    transform: [
                        {
                            rotate: spinP
                        },
                        {
                            translateY: this.state.bottomTransY
                            //translateY: 0
                        },
                        {
                            translateX: this.state.bottomTransX
                            //translateX: -2.5
                        }
                    ]
                }}>

                </Animated.View>
            </View>)
    }
}

BurgerChevron.propTypes = {

};

export default BurgerChevron;
