import React from 'react';
import { View, StyleSheet, Text, Button, TouchableWithoutFeedback, TouchableNativeFeedback, TextInput, Dimensions, ScrollView, Animated, Easing } from 'react-native';
import PropTypes from 'prop-types'

let styles = StyleSheet.create({
    cross: {
        marginRight: 5
    },
    crossUp: {
      backgroundColor: 'red',
      height: 14,
      width: 2
    },
    crossFlat: {
      backgroundColor: 'red',
      height: 2,
      width: 15,
      position: 'absolute',
      left: -7,
      top: 6
    }
});

class Cross extends React.Component {
    constructor(props) {
        super(props);

        this.state = {

        };

        this.onDimensionChange = this.onDimensionChange.bind(this);
        //Dimensions.addEventListener('change', this.onDimensionChange);
    }

    onDimensionChange() {
        // To trigger redraw
        this.setState(Object.assign({}, this.state, {}));
    }

    componentWillUnmount() {
        //Dimensions.removeEventListener('change', this.onDimensionChange);
    }

    render() {
        var { height, width } = Dimensions.get('window');

        return (
            <View style={styles.cross}>
              <View style={StyleSheet.flatten([styles.crossUp, {
                  backgroundColor: this.props.color
              }])} />
              <View style={StyleSheet.flatten([styles.crossFlat, {
                  backgroundColor: this.props.color
              }])} />
            </View>   
          )
    }
}

export default Cross;

Cross.propTypes = {
    color: PropTypes.string
};
