import React from 'react';
import { View, StyleSheet, FlatList, Text, Button, TextInput, Dimensions, ScrollView, Animated, Easing, TouchableWithoutFeedback } from 'react-native';
import PropTypes from 'prop-types'

import CategoryItem from './category-item'

import * as actions from './../../../redux/checkout/data/action'

class CategorySelector extends React.Component {
    constructor(props) {
        super(props);

        let categories = [
            {
                name: 'Pizza',
                subText: '25 items',
                key: 'piz',
                image: {uri: 'https://s3.eu-central-1.amazonaws.com/gergo-aws-teszt/data/img/checkout/pizza.png'},
            },
            {
                name: 'Salads',
                subText: '30 items',
                image: require('./../../../img/checkout/plate-salads.png'),
                key: 'sal'
            },
            {
                name: 'Desserts',
                subText: '30 items',
                image: require('./../../../img/checkout/plate-dessert.png'),
                key: 'des'
            },
            {
                name: 'Pasta',
                subText: '44 item',
                image: require('./../../../img/checkout/plate-pasta.png'),
                key: 'pas'
            },
            {
                name: 'Beverages',
                subText: '30 items',
                image: require('./../../../img/checkout/plate-beverages.png'),
                key: 'bev'
            },
            {
                name: 'Beverages',
                subText: '30 items',
                image: require('./../../../img/checkout/plate-beverages.png'),
                key: 'bev'
            },
            {
                name: 'Beverages',
                subText: '30 items',
                image: require('./../../../img/checkout/plate-beverages.png'),
                key: 'bev'
            }
        ];

        this.state = {
            categories
        };

        this.onDimensionChange = this.onDimensionChange.bind(this);
        //Dimensions.addEventListener('change', this.onDimensionChange);
    }

    onDimensionChange() {
        // To trigger redraw
        this.setState(Object.assign({}, this.state, {}));
    }

    componentWillUnmount() {
        //Dimensions.removeEventListener('change', this.onDimensionChange);
    }

    componentDidMount() {
        let { dispatch } = this.props;
        dispatch(actions.fetchData());
    }

    _onItemPress(item) {
        this.props.requestSwitch('next', item);

        let { dispatch } = this.props;
        dispatch(actions.selectedCategory(item.item.options, item.item.name, item.item.key))
        //alert(JSON.stringify(item.item.options));
    }

    _renderCategoryItems(item) {
        return (
            <CategoryItem pressCallback={() => this._onItemPress(item)} {...item.item}></CategoryItem>
        );
    }

    render() {
        return (
            <View>
                <FlatList
                    data={this.props.data.items}
                    renderItem={this._renderCategoryItems.bind(this)}
                    extraData={this.state}
                />
            </View>)
    }
}

export default CategorySelector;

CategorySelector.propTypes = {
    requestSwitch: PropTypes.func
};
