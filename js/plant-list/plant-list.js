import React from 'react';
import { View, Text, Button, TextInput, Dimensions, ScrollView, FlatList, StyleSheet, TouchableOpacity } from 'react-native';
import Header from './../plant-defaults/header'
import DropShadowView from './../native/drop-shadow'
import DropDownItem from './../plant-defaults/drop-down-item'
import Footer from './../plant-defaults/footer'

import { scale, moderateScale, verticalScale } from './../scaling';
import defaultStyles from './../../styles'
import styles from './styles'

class PlantDefaults extends React.Component {

    static navigatorStyle = {
        drawUnderNavBar: true,
        navBarTranslucent: true,
        navBarHidden: true
    };

    constructor(props) {
        super(props);

        let dropDownOptions = [];
        dropDownOptions.push({ key: 0, title: 'Catalogue', icon: require('./../../img/orangery/tools.png') });
        dropDownOptions.push({ key: 1, title: 'Inspirations', icon: require('./../../img/orangery/recipes.png') });
        dropDownOptions.push({ key: 2, title: 'Tips', icon: require('./../../img/orangery/tools.png') });
        dropDownOptions.push({ key: 3, title: 'Recipes', icon: require('./../../img/orangery/recipes.png') });

        let listChars = ['a', 'b', 'c', 'd', 'e', 'f', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 'w'];

        let plantList = ['Acanthaceae','Achariaceae','Achatocarpaceae','Acoraceae','Actinidiaceae','Adoxaceae','Aextoxicaceae','Aizoaceae',
            'Akaniaceae','Alismataceae','Alseuosmiaceae','Alstroemeriaceae','Altingiaceae','Amaranthaceae','Amaryllidaceae',
            'Amborellaceae','Anacampserotaceae','Anacardiaceae','Anarthriaceae','Ancistrocladaceae'];

        this.state = {
            dropDownOptions: dropDownOptions,
            dropDownVisible: false,
            listChars: listChars,
            plantList: plantList,
            activeAplhabetIndex: 0
        }
    }

    navBack() {
        this.props.navigator.pop({
            animated: true, // does the pop have transition animation or does it happen immediately (optional)
            animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the pop have different transition animation (optional)
        });
    }

    onDroppDownClick() {
        this.setState(Object.assign({}, this.state, {
            dropDownVisible: !this.state.dropDownVisible
        }))
    }

    _renderItem = ({item}) => {
        return (<Text key="2" style={styles.item}>{item}</Text>);
    }

    onAlphabetItemPress(index) {
        this.setState(Object.assign({}, this.state, {
            activeAplhabetIndex: index
        }))
    }

    render() {
        var dropDownOptions = this.state.dropDownOptions.map(function(object, i) {
            return <DropDownItem {...object}></DropDownItem>;
        });

        var listChars = this.state.listChars.map(function(obj, i) {
            let isActive = i == this.state.activeAplhabetIndex ? true : false;
            let style = isActive ? StyleSheet.flatten([styles.scrollViewAlphabet, styles.activeItemAlphabetItem]) : styles.scrollViewAlphabet;
            return (
                <TouchableOpacity style={style} onPress={()=> this.onAlphabetItemPress(i)}>
                    <Text style={{fontSize: scale(18), lineHeight: verticalScale(20), fontFamily: 'cronusRound',}}>{obj.toUpperCase()}</Text>
                </TouchableOpacity>)
        }, this);

        return (
            <View style={{ flex: 1, flexDirection: 'column', backgroundColor: '#FEFEFE' }}>
                <Header droppDownClickHandler={() => this.onDroppDownClick()} />

                <View style={{ flex: 0 }}>
                    <ScrollView horizontal={true} style={{ height: 60 }}>
                        {listChars}
                    </ScrollView>
                </View>

                <View style={{ flex: 1 }}>
                    <FlatList
                        data={this.state.plantList}
                        renderItem={this._renderItem}
                        extraData={this.state}
                    />
                </View>

                { this.state.dropDownVisible ? 
                    <DropShadowView style={defaultStyles.droppShadow} />
                : null }

                { this.state.dropDownVisible ? 
                    <View style={defaultStyles.droppDown}>
                        {dropDownOptions}
                    </View>
                : null }

                <Footer navBack={() => this.navBack()} />
            </View>
        )
    }
}

export default PlantDefaults;