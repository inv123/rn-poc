import { Navigation } from 'react-native-navigation';

import { registerScreens } from './screens';

import configureStore from './js/store/configureStore';
import { Provider } from 'react-redux';

const store = configureStore();

registerScreens(store, Provider); // this is where you register all of your app's screens

// start the app
Navigation.startSingleScreenApp({
    screen: {
        screen: 'main.index', // unique ID registered with Navigation.registerScreen
        title: 'Index main', // title of the screen as appears in the nav bar (optional)
        navigatorStyle: {}, // override the navigator style for the screen, see "Styling the navigator" below (optional)
        navigatorButtons: {} // override the nav buttons for the screen, see "Adding buttons to the navigator" below (optional)
    },
    drawer: {
        // optional, add this if you want a side menu drawer in your app
        left: {
            screen: 'example.drawer',
        }
    },
    passProps: {}, // simple serializable object that will pass as props to all top screens (optional)
    animationType: 'slide-down' // optional, add transition animation to root change: 'none', 'slide-down', 'fade'
});
