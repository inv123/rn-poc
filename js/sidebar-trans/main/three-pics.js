import React from 'react';
import { View, StyleSheet, Text, Button, Image, FlatList, TouchableWithoutFeedback, TouchableNativeFeedback, TextInput, Dimensions, ScrollView, Animated, Easing } from 'react-native';
import PropTypes from 'prop-types'

import { scale, verticalScale } from 'appteszt/js/scaling';

import styles from './styles'

class ThreePicsItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {

        };

        this.onDimensionChange = this.onDimensionChange.bind(this);
        //Dimensions.addEventListener('change', this.onDimensionChange);
    }

    onDimensionChange() {
        // To trigger redraw
        this.setState(Object.assign({}, this.state, {}));
    }

    componentWillUnmount() {
        //Dimensions.removeEventListener('change', this.onDimensionChange);
    }

    render() {
        var { height, width } = Dimensions.get('window');

        // const threePicsItem = {
        //     text: 'Comment on the importance of human life',
        //     topics: 'Fashion',
        //     commentCount: 221,
        //     image1: require('appteszt/img/sidebar/img5.png'),
        //     image2: require('appteszt/img/sidebar/img6.png'),
        //     image3: require('appteszt/img/sidebar/img5.png')
        // };

        return (
            <View style={{ height: verticalScale(220), marginRight: 5, marginLeft: 5 }}>
                <TouchableNativeFeedback onPress={() => this.props.changeView('detailsNd')}>
                    <View style={{ flex: 1, flexDirection: 'column' }}>
                        <View style={{ marginLeft: 5 }}>
                            <Text style={styles.titleText}>{this.props.text}</Text>
                        </View>
                        <View style={{ flex: 6, flexDirection: 'row', marginTop: 5,
                            justifyContent: 'space-between' }}>
                            <Image style={styles.tsImage} source={this.props.image1}></Image>
                            <Image style={styles.tsImage} source={this.props.image2}></Image>
                            <Image style={styles.tsImage} source={this.props.image3}></Image>
                        </View>
                        <View style={{ flex: 2, flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ marginLeft: 5 }}>
                                <Text style={styles.typeText}>{this.props.topic.toUpperCase()}</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end' }}>
                                <Image style={{ marginTop: 0, transform: [{ scale: 0.9 }] }} source={require('appteszt/img/sidebar/ico_text.png')}></Image>
                                <Text style={{ fontSize: 14 }}>{this.props.commentCount}</Text>
                            </View>
                        </View>
                    </View>
                </TouchableNativeFeedback>
            </View>)
    }
}

//CategorySelector.propTypes = {
//};

export default ThreePicsItem;
