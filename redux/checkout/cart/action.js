export const ADDITEM = 'addToCart'
export const ALTERQUANTITY = 'alterQuantity'
export const CHECKOUT = 'checkoutCart'

export function AddToCart(optionItem, categoryKey) {
    return { type: ADDITEM, optionItem, categoryKey };
}
export function AlterQuantity(optionItem) {
    return { type: ALTERQUANTITY, optionItem };
}
export function Checkout() {
    return { type: CHECKOUT };
}
