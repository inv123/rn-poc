import React from 'react';
import { View, StyleSheet, Text, Button, Image, FlatList, TouchableWithoutFeedback, TouchableNativeFeedback, TextInput, Dimensions, ScrollView, Animated, Easing } from 'react-native';
import PropTypes from 'prop-types'

import { scale, verticalScale } from 'appteszt/js/scaling';

class HrLine extends React.Component {
    constructor(props) {
        super(props);

        this.state = {

        };

        this.onDimensionChange = this.onDimensionChange.bind(this);
        //Dimensions.addEventListener('change', this.onDimensionChange);
    }

    onDimensionChange() {
        // To trigger redraw
        this.setState(Object.assign({}, this.state, {}));
    }

    componentWillUnmount() {
        //Dimensions.removeEventListener('change', this.onDimensionChange);
    }

    render() {
        var { height, width } = Dimensions.get('window');

        return (
            <View style={{ marginTop: 5, marginBottom: 5 }}>
                <View style={{ backgroundColor: '#f7f7f7', height: 1 }}>
                </View>
                <View style={{ backgroundColor: '#f2f2f2', height: 1 }}>
                </View>
            </View>)
    }
}

//CategorySelector.propTypes = {
//};

export default HrLine;
