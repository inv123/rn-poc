import React from 'react';
import { View, StyleSheet, Text, Button, Image, TextInput, Dimensions, ScrollView, Animated, Easing } from 'react-native';
import PropTypes from 'prop-types'

class OptionRating extends React.Component {
    constructor(props) {
        super(props);

        this.state = {

        };

        //this.onDimensionChange = this.onDimensionChange.bind(this);
        //Dimensions.addEventListener('change', this.onDimensionChange);
    }

    onDimensionChange() {
        // To trigger redraw
        this.setState(Object.assign({}, this.state, {}));
    }

    componentWillUnmount() {
        //Dimensions.removeEventListener('change', this.onDimensionChange);
    }

    starOffImage = require('./../../../img/checkout/star-off-icon.png');
    starOff(key) {
        return (<Image key={key} style={{
            transform: [
                {
                    scale: 1.1
                }
            ]
        }} source={this.starOffImage} />)
    }
    
    starOnImage = require('./../../../img/checkout/star-on-icon.png');
    starOn(key) {
        return (
            <Image key={key} style={{
                transform: [
                    {
                        scale: 1.1
                    }
                ]
            }} source={this.starOnImage} />
        )
    }
    render() {
        let keyNum = 0;
        let stars = [];
        for (let i = 0; i < this.props.rating; i++) {
            stars.push(this.starOn(keyNum++));
        }
        for (let i = 0; i < this.props.outOf-this.props.rating; i++) {
            stars.push(this.starOff(keyNum++));
        }

        return (
            <View style={{
                flex: 1,
                flexDirection: 'row'
            }}>
                {stars}
                {/* <Text>{this.props.rating}/{this.props.outOf}</Text> */}
            </View>)
    }
}

export default OptionRating;

OptionRating.propTypes = {
    rating: PropTypes.number,
    outOf: PropTypes.number
}
