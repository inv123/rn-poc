import React from 'react';
import { View, StyleSheet, Text, Button, Image, FlatList, TouchableWithoutFeedback, TouchableNativeFeedback, TextInput, Dimensions, ScrollView, Animated, Easing } from 'react-native';
import PropTypes from 'prop-types'

import { scale, verticalScale } from 'appteszt/js/scaling';

import Draggable from 'appteszt/js/components/draggable';

class NavPoc extends React.Component {
    constructor(props) {
        super(props);

        this.state = {

        };

        this.onDimensionChange = this.onDimensionChange.bind(this);
        //Dimensions.addEventListener('change', this.onDimensionChange);
    }

    onDimensionChange() {
        // To trigger redraw
        this.setState(Object.assign({}, this.state, {}));
    }

    componentWillUnmount() {
        //Dimensions.removeEventListener('change', this.onDimensionChange);
    }

    onTouchPress() {
        this.props.navigator.push({
            screen: 'navpoc.details',
            title: 'Some title',
            animated: true
          });
    }

    render() {
        var { height, width } = Dimensions.get('window');

        return (
            <View>
                <Text>Main</Text>
                <TouchableNativeFeedback onPress={() => this.onTouchPress()}>
                    <Text>Go to details</Text>
                </TouchableNativeFeedback>
                <View style={{
                    flex: 1,
                    flexDirection: 'row',
                    marginTop: 180
                }}>
                    <Draggable />
                    <Draggable />
                    <Draggable />
                </View>
            </View>)
    }
}

//CategorySelector.propTypes = {
//};

export default NavPoc;
