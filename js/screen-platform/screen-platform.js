import React from 'react';
import { Animated, Easing, View, Text, Button, StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import * as actions from '.././main/actions'
import * as actionsFetch from './actions'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Later on in your styles..
var styles = StyleSheet.create({
  linearGradient: {
    //flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 5
  },
  buttonText: {
    fontSize: 18,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 20,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
});

class ScreenPlatform extends React.Component {
  constructor(props) {
    super(props);

    const { dispatch } = props;
    this.boundActionCreators = bindActionCreators(actions, dispatch);

    //this.onButtonPress = this.onButtonPress.bind(this);
    this.state = {
      aniHeight: new Animated.Value(30)
    }
  }

  componentDidMount() {
    console.log('screen-platform')
  }

  onPress() {
    Animated.timing(this.state.aniHeight, {
      toValue: 300,
      easing: Easing.back(),
      duration: 1000,
    }).start();
  }

  onAddPress() {
    let { dispatch } = this.props;
    dispatch(actions.IncrementVar());
  }

  onFetchMePress() {
    let { dispatch } = this.props;
    dispatch(actionsFetch.fetchPlants())
    .then(() => console.log('finished'));
  }

  render() {
    console.log(this.props.plants.items);
    var items = this.props.plants.items.map((item, i) => {
      return <Text key={item.name}>{item.name} desc.: {item.description}</Text>
    });
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1, flexDirection: 'row', backgroundColor: 'purple', minHeight: 10, maxHeight: 40 }}>
          <View style={{ alignSelf: 'center', flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
            <Text>Screen platform android {this.props.number}</Text>
          </View>
        </View>
        <View style={{ flex: 6, flexDirection: 'column', backgroundColor: 'red' }}>
          <Animated.View style={{ backgroundColor: 'white', height: this.state.aniHeight }}>
            <Button onPress={() => this.onAddPress()} title="Increment me!"></Button>
            <Button onPress={() => this.onFetchMePress()} title="Fetch me!"></Button>
            <Text>Screen platform android 2</Text>
            <Text>Screen platform android 3</Text>
            <Text>isFetching: {this.props.plants.isFetching ? 'true' : 'false' }, date: {new Date(this.props.plants.receivedAt).toString()}</Text>
            {items}
          </Animated.View>
          <Button onPress={()=> this.onPress()} title="Some shit"></Button>
          <View style={{ shadowColor: 'blue', shadowOffset: { width: 1, height: 50 }, shadowRadius: 35, shadowOpacity: 1 }}>
            <Text>Screen platform android 4</Text>
            <Text>Screen platform android shadow</Text>
            <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} style={styles.linearGradient}>
              <Text style={styles.buttonText}>
                Sign in with Facebook
              </Text>
            </LinearGradient>
            <LinearGradient
              start={{x: 0.0, y: 0.0}} end={{x: 1.0, y: 1.0}}
              locations={[0, 0.5, 1.0]}
              colors={['yellow', 'black', 'purple']}
              style={styles.linearGradient}>
              <Text style={styles.buttonText}>
                Sign in with Facebook
              </Text>
            </LinearGradient>
          </View>
        </View>
        <View style={{ flex: 1, backgroundColor: 'green', minHeight: 10, maxHeight: 40 }}>
          <Text style={{ fontFamily: 'cronusRound' }}>Screen platform android</Text>
        </View>
      </View>
    );
  }
}

export default connect(state => ({ number: state.number, plants: state.plants }))(ScreenPlatform);
