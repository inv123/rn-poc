const store = {
    data: {
        categories: [
            {
                name: 'Pizza',
                subText: '25 items',
                key: 'piz',
                image: require('./../../../img/checkout/pizza.png'),
                options: [
                    {
                        name: 'Fried rice with egg and onion',
                        description: 'The secret to the best Chinise fried rice is onions, garlic and sesame oil, you may add in cooked chicken, beef, pork or shrimp, also you may add in some frozen thawed peas or fresh sauteed or canned mushrooms, whatever you have handy in your fridge!',
                        rating: 4,
                        weight: '250 g',
                        price: '8.00 $',
                        image: require('./../../../img/checkout/plate-salads.png'),
                        key: 1
                    },
                    {
                        name: 'Savannah chopped salad',
                        description: 'Bacon ipsum dolor amet meatloaf ball tip bresaola, fatback tongue tail tenderloin rump porchetta beef shankle. Ball tip shoulder ribeye flank short loin biltong frankfurter. Ham hock pig prosciutto, chicken flank sirloin salami turkey.',
                        rating: 3,
                        weight: '300 g',
                        price: '3.00 $',
                        image: require('./../../../img/checkout/plate-salad2.png'),
                        key: 2
                    },
                    {
                        name: 'Autumn carved Turkey salad',
                        description: 'Ham alcatra ribeye porchetta sausage. Pork chop frankfurter rump venison short loin, spare ribs chuck boudin.',
                        rating: 4,
                        weight: '250 g',
                        price: '8.99 $',
                        image: require('./../../../img/checkout/plate-salad3.png'),
                        key: 3
                    }
                ]
            },
            {
                name: 'Salads',
                subText: '30 items',
                image: require('./../../../img/checkout/plate-salads.png'),
                key: 'sal'
            },
            {
                name: 'Desserts',
                subText: '30 items',
                image: require('./../../../img/checkout/plate-dessert.png'),
                key: 'des'
            },
            {
                name: 'Pasta',
                subText: '44 item',
                image: require('./../../../img/checkout/plate-pasta.png'),
                key: 'pas'
            },
            {
                name: 'Beverages',
                subText: '30 items',
                image: require('./../../../img/checkout/plate-beverages.png'),
                key: 'bev'
            },
            {
                name: 'Beverages',
                subText: '30 items',
                image: require('./../../../img/checkout/plate-beverages.png'),
                key: 'bev'
            },
            {
                name: 'Beverages',
                subText: '30 items',
                image: require('./../../../img/checkout/plate-beverages.png'),
                key: 'bev'
            }
        ],
        currency: '$'
    },
    cart: {
        summaryCount: 0,
        // Should be sent to the server
        orders: [
            {
                categoryKey: 1,
                optionKey: 1,
                amount: 0
            },
            {
                categoryKey: 1,
                optionKey: 1,
                amount: 0
            }
        ],
        summaryData: {
            numberOfItems: 3,
            subTotal: 25.75,
            tax: 1,
            total: 26.75
        }
    }
};

export default store;
