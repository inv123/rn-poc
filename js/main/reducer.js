import { combineReducers } from 'redux'
import * as actions from './actions'

// Got only the number as state
function number(state = 0, action) {
    switch (action.type) {
        case actions.INCREMENT:
            return state + 1;
            break;
        case actions.DECREMENT:
            return state - 1;
            break;
        case actions.RESET:
            return action.param;
            break;
        default:
            return state;
    }
}
  
export default number
