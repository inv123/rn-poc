import React from 'react';
import { View, StyleSheet, Text, Button, Image, FlatList, TouchableWithoutFeedback, TouchableNativeFeedback, TextInput, Dimensions, ScrollView, Animated, Easing } from 'react-native';
import PropTypes from 'prop-types'

import SideBarItem from 'appteszt/js/sidebar-trans/sidebar-item'
import Main from 'appteszt/js/sidebar-trans/main/main'
import Details from 'appteszt/js/sidebar-trans/details/details'
import DetailsNd from 'appteszt/js/sidebar-trans/details/details-nd'
import Comment from 'appteszt/js/sidebar-trans/comment/comment'

import { scale, verticalScale } from 'appteszt/js/scaling';

class SideBarTrans extends React.Component {
    static navigatorStyle = {
        drawUnderNavBar: true,
        navBarTranslucent: true,
        navBarHidden: true
    };

    constructor(props) {
        super(props);

        this.state = {
            currentStage: 'main',

            navBarVisible: false,

            translate: new Animated.Value(0),
            scale: new Animated.Value(1),

            options: [
                {
                    key: 1,
                    image: require('appteszt/img/sidebar/recommended.png'),
                    text: 'Recommended'
                },
                {
                    key: 1,
                    image: require('appteszt/img/sidebar/politics.png'),
                    text: 'Politics'
                },
                {
                    key: 1,
                    image: require('appteszt/img/sidebar/sports.png'),
                    text: 'Sports'
                },
                {
                    key: 1,
                    image: require('appteszt/img/sidebar/events.png'),
                    text: 'Events'
                },
                {
                    key: 1,
                    image: require('appteszt/img/sidebar/science.png'),
                    text: 'Technology'
                },
                {
                    key: 1,
                    image: require('appteszt/img/sidebar/travel.png'),
                    text: 'Travel'
                },
                {
                    key: 1,
                    image: require('appteszt/img/sidebar/location.png'),
                    text: 'Local'
                },
                {
                    key: 1,
                    image: require('appteszt/img/sidebar/lightbulp.png'),
                    text: 'Science'
                }
            ],

            detailsPageContent: {
                title: 'Tips For Preventing And Controlling High Blood Pressure',
                author: 'Leah Parks',
                date: '09 Dec 2017',

                tags: [
                    {
                        text: 'Science'
                    },
                    {
                        text: 'Woman'
                    },
                    {
                        text: 'USA'
                    },
                    {
                        text: 'Some'
                    },
                    {
                        text: 'Other tags'
                    },
                    {
                        text: 'Other than'
                    },
                    {
                        text: 'USA and Woman And Science'
                    },
                    {
                        text: 'Great'
                    },
                    {
                        text: 'Good stuff'
                    }
                ]
            }
        };

        this.onDimensionChange = this.onDimensionChange.bind(this);
        Dimensions.addEventListener('change', this.onDimensionChange);
    }

    onDimensionChange() {
        // To trigger redraw
        this.setState(Object.assign({}, this.state, {}));
    }

    componentWillUnmount() {
        Dimensions.removeEventListener('change', this.onDimensionChange);
    }
    
    onSideThingPress() {
        //alert('side')
        Animated.timing(this.state.scale, {
            toValue: 0.85,
            easing: Easing.out(Easing.exp),
            duration: 500,
        }).start();
        Animated.timing(this.state.translate, {
            toValue: 160,
            easing: Easing.out(Easing.exp),
            duration: 500,
        }).start();

        this.setState(Object.assign({}, this.state, {
            navBarVisible: true
        }))
    }

    onMainViewPress() {
        //alert('ala sjad')
        if (this.state.navBarVisible) {
            Animated.timing(this.state.scale, {
                toValue: 1,
                easing: Easing.out(Easing.exp),
                duration: 500,
            }).start();
            Animated.timing(this.state.translate, {
                toValue: 0,
                easing: Easing.out(Easing.exp),
                duration: 500,
            }).start();

            this.setState(Object.assign({}, this.state, {
                navBarVisible: false
            }))
        }
    }

    shouldresponse() {
        if (this.state.navBarVisible) {
            return false;
        }
        return true;
    }

    _renderSideBarItem(item) {
        return (
            <SideBarItem {...item}></SideBarItem>
        );
    }

    changeView(newState) {
        this.setState(Object.assign({}, { currentStage: newState }, {}));
    }

    render() {
        var { height, width } = Dimensions.get('window');

        const sideBarItems = this.state.options.map((item, i) => {
            return this._renderSideBarItem(item);
        });

        const views = {
            main: () => {
                return (<Main showSide={() => this.onSideThingPress()} changeView={(v) => this.changeView(v)}></Main>)
            },
            details: () => {
                return (<Details showSide={() => this.onSideThingPress()} changeView={(v) => this.changeView(v)} data={this.state.detailsPageContent}></Details>)
            },
            detailsNd: ()=> {
                return (<DetailsNd />);
            },
            comment: () => {
                return (<Comment changeView={(v) => this.changeView(v)}></Comment>);
            }
        };

        return (
            <View style={{ backgroundColor: '#f8f8f8', flex: 1, margin: 0 }}>
                {/* Side bar */}
                <View style={{
                    zIndex: 1,
                    paddingTop: 50,
                    marginLeft: 10,
                    marginBottom: 20
                }}>
                    <ScrollView showsVerticalScrollIndicator={true}>
                        {sideBarItems}
                    </ScrollView>
                </View>

                {/* Main view */}
                <TouchableWithoutFeedback onPress={() => this.onMainViewPress()}>
                    <Animated.View style={{
                        zIndex: 2,
                        backgroundColor: 'white',
                        left: 0,
                        top: 0,
                        //flex: 1,
                        height: height - scale(25),
                        //width: width,
                        position: 'absolute',
                        elevation: 35,
                        transform:
                        [
                            {
                                translateX: this.state.translate
                            },
                            {
                                scale: this.state.scale
                            }
                        ]
                    }}>
                        <View onStartShouldSetResponder={() => this.shouldresponse()} style={{ flex: 1 }}>
                            {views[this.state.currentStage]()}
                            {/* <Main showSide={() => this.onSideThingPress()}></Main> */}
                        </View>
                    </Animated.View>
                </TouchableWithoutFeedback>
            </View>)
    }
}

//CategorySelector.propTypes = {
//};

export default SideBarTrans;
