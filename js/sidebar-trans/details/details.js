import React from 'react';
import { View, StyleSheet, Text, Button, Image, FlatList, TouchableWithoutFeedback, TouchableNativeFeedback, TextInput, Dimensions, ScrollView, Animated, Easing, WebView } from 'react-native';
import PropTypes from 'prop-types'

import LinearGradient from 'react-native-linear-gradient';

import HrLine from 'appteszt/js/components/hr-line'
import Chevron from 'appteszt/js/components/chevron'
import DropShadowView from 'appteszt/js/native/drop-shadow'
import VideoItem from 'appteszt/js/sidebar-trans/main/video-item'

import { scale, verticalScale } from 'appteszt/js/scaling';

import styles from './styles'

class Details extends React.Component {
    constructor(props) {
        super(props);

        this.state = {

        };

        this.onDimensionChange = this.onDimensionChange.bind(this);
        //Dimensions.addEventListener('change', this.onDimensionChange);
    }

    onDimensionChange() {
        // To trigger redraw
        this.setState(Object.assign({}, this.state, {}));
    }

    componentWillUnmount() {
        //Dimensions.removeEventListener('change', this.onDimensionChange);
    }

    render() {
        var { height, width } = Dimensions.get('window');

        const videoItem = {
            text: 'Comment on the importance of human life',
            type: 'Local',
            commentCount: 45,
            image: require('appteszt/img/sidebar/img4.png')
        };

        const tagViews = this.props.data.tags.map((item, i) => {
            return (
                <View style={{
                    padding: 5,
                    paddingTop: 2,
                    paddingLeft: 15,
                    paddingRight: 15,
                    backgroundColor: '#e5e5e5',
                    marginTop: 8,
                    marginLeft: 8,
                    
                    borderTopRightRadius: 5,
                    borderTopLeftRadius: 5,
                    borderBottomLeftRadius: 5,
                    borderBottomRightRadius: 5,
                    alignSelf: 'flex-start'
                }}>
                    <Text style={{ textAlign: 'center', color: '#737373' }}>{item.text}</Text>
                </View>
            );
        });

        return (
            <View style={{ flex: 1 }}>
                <ScrollView style={{ flex: 1 }}>
                    <LinearGradient style={{ flex: 1 }} colors={['rgba(0, 0, 0, 0.4)', 'rgba(0, 0, 0, 0.3)','rgba(0, 0, 0, 0.2)', 'transparent']}>
                        <View style={{
                                flexDirection: 'row', justifyContent: 'space-between',
                                alignItems: 'center',
                                height: verticalScale(70),
                            }}>
                        
                            <View style={{ width: 10, marginLeft: 20, marginTop: 8,
                            height: 25,
                            transform:[
                                {
                                    rotate: '180deg'
                                },
                                {
                                    scale: 1.5
                                }
                            ] }}>
                                <TouchableNativeFeedback onPress={() => this.props.changeView('main')}>
                                    <View style={{ flex: 1, paddingTop: 5 }}>
                                        <Chevron color='#1f1f1f'></Chevron>
                                    </View>
                                </TouchableNativeFeedback>
                            </View>
                        </View>
                    </LinearGradient>

                    <View style={{ flex: 1 }}>
                        <Text style={{ fontSize: 30, color: '#1f1f1f', fontWeight: 'bold', marginLeft: 10, marginRight: 10 }}>
                            {this.props.data.title}
                        </Text>
                    </View>

                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', marginLeft: 5, marginRight: 5, marginTop: 15 }}>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <View style={{ marginRight: 8 }}>
                                <Image source={require('appteszt/img/sidebar/travel.png')}></Image>
                            </View>
                            <View>
                                <View>
                                    <Text style={{ fontWeight: 'bold' }}>{this.props.data.author}</Text>
                                </View>
                                <View>
                                    <Text>{this.props.data.date}</Text>
                                </View>
                            </View>
                        </View>

                        <View>
                            <TouchableNativeFeedback>
                                <View style={{
                                    backgroundColor: '#fe5569',
                                    padding: 8,
                                    borderTopRightRadius: 25,
                                    borderTopLeftRadius: 25,
                                    borderBottomLeftRadius: 25,
                                    borderBottomRightRadius: 25
                                    }}>
                                    <Text style={{ color: '#f0b1bf' }}>SUBSCRIBE</Text>
                                </View>
                            </TouchableNativeFeedback>
                        </View>
                    </View>

                    <View>
                        <Text style={styles.detailsText}>Spicy jalapeno bacon ipsum dolor amet corned beef leberkas strip steak sirloin salami meatloaf capicola tongue ground round ball tip hamburger cow andouille turkey.</Text>
                        <View style={{
                            margin: 8,
                            alignItems: 'center',
                        }}>
                            <Image style={{
                                
                            }} source={require('appteszt/img/sidebar/img3.png')}></Image>
                        </View>
                        {/* <WebView
                            source={{ html: "<p style='text-align: justify;'>Justified text here</p>" }}
                        /> */}
                        <Text style={styles.detailsText}>Meatloaf shoulder short ribs venison salami boudin chuck pork chop. Kielbasa tri-tip fatback pancetta, pork chop prosciutto meatloaf venison strip steak corned beef alcatra turducken beef ribs. Spare ribs shankle frankfurter brisket meatloaf swine cow. Drumstick ball tip landjaeger t-bone. Filet mignon beef boudin pastrami cupim pork chop buffalo shoulder ham hock. Drumstick pig leberkas tongue ham hock sirloin.</Text>
                    </View>

                    <View style={{
                        flex: 1,
                        flexWrap: 'wrap',
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginBottom: 10
                    }}>
                        {tagViews}
                    </View>

                    <View style={{ height: 20, backgroundColor: '#f8f8f8' }}>
                    </View>

                    <View>
                        <View style={{
                            marginTop: 8,
                            marginBottom: 15
                        }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 22, color: '#1f1f1f', marginLeft: 8 }}>Related</Text>
                        </View>
                        <View>
                            <VideoItem changeView={()=> {}} {...videoItem}></VideoItem>
                            <HrLine />
                            <VideoItem changeView={()=> {}} {...videoItem}></VideoItem>
                            <HrLine />
                            <VideoItem changeView={()=> {}} {...videoItem}></VideoItem>
                        </View>
                    </View>

                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        backgroundColor: '#f8f8f8',
                        paddingTop: 15,
                        height: verticalScale(120),
                        marginTop: 15
                    }}>
                        <View style={{
                            marginLeft: 25,
                            alignItems: 'center'
                        }}>
                            <Image source={require('appteszt/img/sidebar/ico_ok.png')} />
                            <Text style={styles.redText}>199</Text>
                        </View>

                        <View style={{
                            marginLeft: 10,
                            alignItems: 'center'
                        }}>
                            <Image source={require('appteszt/img/sidebar/ico_star.png')} />
                            <Text style={styles.normalBottomText}>FACORITE+</Text>
                        </View>

                        <TouchableNativeFeedback onPress={() => this.props.changeView('comment')}>
                            <View style={{
                                marginLeft: 10,
                                alignItems: 'center'
                            }}>
                                <Image source={require('appteszt/img/sidebar/ico_comment.png')} />
                                <Text style={styles.redText}>2901+</Text>
                            </View>
                        </TouchableNativeFeedback>

                        <View style={{
                            marginLeft: 10,
                            marginRight: 25,
                            alignItems: 'center'
                        }}>
                            <Image source={require('appteszt/img/sidebar/ico_share.png')} />
                            <Text style={styles.normalBottomText}>SHARE</Text>
                        </View>
                    </View>
                </ScrollView>
            </View>)
    }
}

//CategorySelector.propTypes = {
//};

export default Details;
