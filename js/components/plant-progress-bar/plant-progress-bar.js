import React from 'react';
import { View, Text, Button, TextInput, Dimensions } from 'react-native';
import PropTypes from 'prop-types'

import styles from './styles'

class PlantProgressBar extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let baseLineColor = this.props.baseLineColor;
        let baseLineOffColor = this.props.baseLineOffColor;

        let p1 = this.props.progress;
        let progress = p1 == null || p1 == undefined || p1 == 0 ? 0 : p1;

        let margin = 10;
        let verticalLinesOffSet = 35;
        let offColorLineWidth = verticalLinesOffSet * 4;
        let baseColorWidth = (progress / 100) * offColorLineWidth;

        return (
        <View style={{ marginBottom: margin }}>
            <View style={{ borderRightColor: baseLineOffColor, borderRightWidth: 0.4, height: 20, width: verticalLinesOffSet }}>
            </View>
            <View style={{ borderBottomColor: baseLineOffColor, borderBottomWidth: 2, height: 1, width: offColorLineWidth, marginTop: -1 * margin }}>
            </View>
            <View style={{ borderRightColor: baseLineOffColor, borderRightWidth: 0.4, height: 20, width: verticalLinesOffSet * 2, marginTop: (-1 * margin) -2 }}>
            </View>
            <View style={{ borderRightColor: baseLineOffColor, borderRightWidth: 0.4, height: 20, width: verticalLinesOffSet * 3, marginTop: (-2 * margin) }}>
            </View>
            
            <View style={{ borderBottomColor: baseLineColor, borderBottomWidth: 2, height: 1, width: baseColorWidth, marginTop: -1 * margin }}>
            </View>
        </View>)
    }
}

export default PlantProgressBar;

PlantProgressBar.propTypes = {
    baseLineColor: PropTypes.string,
    baseLineOffColor: PropTypes.string,
    progress: PropTypes.number // todo validate range
};
