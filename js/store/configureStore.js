import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './../rootReducer';
//import {createLogger} from 'redux-logger';

let middleware = [thunk];

// if (__DEV__) {
// 	// const reduxImmutableStateInvariant = require('redux-immutable-state-invariant')();
// 	//const logger = createLogger({ collapsed: true });
// 	//middleware = [...middleware];
// } else {
// 	middleware = [...middleware];
// }
middleware = [...middleware];

export default function configureStore(initialState) {
	return createStore(
		rootReducer,
		initialState,
		applyMiddleware(...middleware)
	);
}