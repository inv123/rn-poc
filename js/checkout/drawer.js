import React from 'react';
import { Text, Button, StyleSheet, View } from 'react-native';

class MyClass extends React.Component {
  onShowModal = () => {
    //this.toggleDrawer();
    this.props.navigator.toggleDrawer({
      side: 'left', // the side of the drawer since you can have two, 'left' / 'right'
      animated: true,
    });
    this.props.navigator.showModal({
      screen: 'example.login',
      title: 'Login'
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={{ height: 120}}>
          <Text>Head</Text>
        </View>
        <View style={{ flex: 1 }}>
          <Text>Middle</Text>
          <Button onPress={this.onShowModal} title="Login"></Button>
        </View>
        <View style={{ height: 80 }}>
          <Text>Bottom</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: 120,
    alignItems: 'center',
    //justifyContent: 'center',
    backgroundColor: '#ffffff',
  },
  button: {
    marginTop: 16
  }
});

export default MyClass;
