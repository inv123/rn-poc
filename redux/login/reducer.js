import * as actions from './action'

const initialValue = {
    loginInProgress: false,
    receivedAt: Date.now(),
    responseData: {

    }
}

function Login(state = initialValue, action) {
    console.log(state, actions)
    switch(action.type) {
        case actions.LOGINREQUESTOK:

            return Object.assign({}, state, 
                { responseData: actions.responseData, receivedAt: actions.receivedAt }
            );
        break;
        case actions.REQUESTSTATUSCHANGED:

            return Object.assign({}, state, 
                { loginInProgress: actions.loginInProgress, receivedAt: actions.receivedAt }
            );
        break;

        default:
            return state;
    }
}

export default Login;
