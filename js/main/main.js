import React from 'react';
import { View, Text, Button, requireNativeComponent, ScrollView } from 'react-native';
//var MyBasicView = requireNativeComponent('MyBasicView', MyBasicView);
//var DropShadowView = requireNativeComponent('ShadowDroppView', DropShadowView);
import DropShadowView from './../native/drop-shadow'
import style from './../../styles'

// Redux
// import { createStore } from 'redux'
// import rootReducer from './../rootReducer'
import * as actions from './actions'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
//let store = createStore(rootReducer)

class Main extends React.Component {
  constructor(props) {
    super(props); 
    
    //this.onButtonPress = this.onButtonPress.bind(this);

    const { dispatch } = props;
    
    // Here's a good use case for bindActionCreators:
    // You want a child component to be completely unaware of Redux.
    // We create bound versions of these functions now so we can
    // pass them down to our child later.

    this.boundActionCreators = bindActionCreators(actions, dispatch)
    // console.log(this.boundActionCreators)
  }

  componentDidMount() {
    // Injected by react-redux:
    let { dispatch } = this.props
    
    // This will work:
    // let action = TodoActionCreators.addTodo('Use Redux')
    // dispatch(action);
    // dispatch(actions.IncrementVar())
    // dispatch(actions.IncrementVar())
    // dispatch(actions.IncrementVar())
  }

  onButtonPress(screen) {
    console.log(screen);
    this.props.navigator.push({
      screen: screen,
      title: 'Some title',
      animated: true
    });
  }

  render() {
    let { number } = this.props
    return (
      <ScrollView>
        {/* <Text style={style.container}>
          Some text wasupp {number}
        </Text> */}
        <Button
        onPress={()=> this.onButtonPress('serieslist.index')}
        title="Next page"
        />

        <Button
        onPress={()=> this.onButtonPress('screenPlatform.index')}
        title="Screen platform"
        />

        <Button
        onPress={()=> this.onButtonPress('headerDesign.index')}
        title="Header design"
        />

        <Button
        onPress={()=> this.onButtonPress('plants.list')}
        title="Plant list"
        />

        <Button
        onPress={()=> this.onButtonPress('plants.default')}
        title="Plant default"
        />

        <Button
        onPress={()=> this.onButtonPress('plants.details')}
        title="Plant details"
        />
        
        <Button
        onPress={()=> this.onButtonPress('example.checkout')}
        title="Checkout"
        />

        {/* <Button
        onPress={()=> this.onButtonPress('example.backgroundChild')}
        title="Background child"
        /> */}
        <Button
        onPress={()=> this.onButtonPress('example.sidebarTrans')}
        title="Side bar"
        />

        <Button
        onPress={()=> this.onButtonPress('navpoc.main')}
        title="Nav poc"
        />

        {/* <DropShadowView style={{ width: 350, height: 250, position: 'absolute', marginTop: 120 }} /> */}
        {/* <MyBasicView style={{ width: 100, height: 100 }} /> */}
      </ScrollView>
    );
  }
}

export default connect(
  state => {
    return ({ number: state.number })
  }
)(Main);