import React from 'react';
import { View, StyleSheet, FlatList, Text, Button, TextInput, Dimensions, ScrollView, Animated, Easing } from 'react-native';

import OptionItems from './option-item'
import * as actions from './../../../redux/checkout/cart/action'

import { scale } from '../../scaling';

class OptionList extends React.Component {
    constructor(props) {
        super(props);

        let catItems = [
            {
                name: 'Fried rice with egg and onion',
                description: 'The secret to the best Chinise fried rice is onions, garlic and sesame oil, you may add in cooked chicken, beef, pork or shrimp, also you may add in some frozen thawed peas or fresh sauteed or canned mushrooms, whatever you have handy in your fridge!',
                rating: 4,
                weight: '250 g',
                price: '8.00 $',
                image: require('./../../../img/checkout/plate-salads.png'),
                key: 1
            },
            {
                name: 'Savannah chopped salad',
                description: 'Bacon ipsum dolor amet meatloaf ball tip bresaola, fatback tongue tail tenderloin rump porchetta beef shankle. Ball tip shoulder ribeye flank short loin biltong frankfurter. Ham hock pig prosciutto, chicken flank sirloin salami turkey.',
                rating: 3,
                weight: '300 g',
                price: '3.00 $',
                image: require('./../../../img/checkout/plate-salad2.png'),
                key: 2
            },
            {
                name: 'Autumn carved Turkey salad',
                description: 'Ham alcatra ribeye porchetta sausage. Pork chop frankfurter rump venison short loin, spare ribs chuck boudin.',
                rating: 4,
                weight: '250 g',
                price: '8.99 $',
                image: require('./../../../img/checkout/plate-salad3.png'),
                key: 3
            }
        ];

        this.state = {
            catItems
        };

        this.onDimensionChange = this.onDimensionChange.bind(this);
        //Dimensions.addEventListener('change', this.onDimensionChange);
    }

    onDimensionChange() {
        // To trigger redraw
        this.setState(Object.assign({}, this.state, {}));
    }

    componentWillUnmount() {
        //Dimensions.removeEventListener('change', this.onDimensionChange);
    }

    _onItemPress(item) {
        //alert(`${JSON.stringify(item)}, ${this.props.data.selectedCategory.key}`);

        const { dispatch } = this.props;
        dispatch(actions.AddToCart(item, this.props.data.selectedCategory.key));
    }

    _renderCategoryOptions(item) {
        return (
            <OptionItems pressCallback={() => this._onItemPress(item.item)} {...item.item} data={this.props.data}></OptionItems>
        );
    }

    render() {
        const items = this.props.data.categoryOptions.map((object, i) => {
            return this._renderCategoryOptions({ item: object });
        });

        return (
            <View>
                {/* <Text>1234{this.props.data.isFetching ? 'true' : 'false'}, {this.props.data.categoryOptions.length}</Text> */}
                <ScrollView>
                    <View>
                        <Text style={{ fontSize: scale(28), paddingLeft: 40, paddingRight: 150, paddingBottom: 20, backgroundColor: 'transparent' }}>{this.props.data.selectedCategory.name} menu</Text>
                    </View>
                    {items}
                </ScrollView>
            </View>)
    }
}

export default OptionList;
