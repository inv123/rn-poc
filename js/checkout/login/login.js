import React from 'react';
import { View, StyleSheet, Text, Button, Image, FlatList, TouchableWithoutFeedback, TouchableNativeFeedback, TextInput, Dimensions, ScrollView, Animated, Easing } from 'react-native';
import PropTypes from 'prop-types'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from 'appteszt/redux/checkout/data/action'

class Login extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: ''
        };

        this.onDimensionChange = this.onDimensionChange.bind(this);
        //Dimensions.addEventListener('change', this.onDimensionChange);
    }

    onLoginPress = () => {
        // this.props.navigator.toggleDrawer({
        //     side: 'left',
        //     animated: true
        // });
        this.props.navigator.dismissModal();
        //Navigation.dismissModal({
    };

    onChangeText(data) {
        this.setState(Object.assign({}, this.state, data));
    }

    onDimensionChange() {
        // To trigger redraw
        this.setState(Object.assign({}, this.state, {}));
    }

    componentWillUnmount() {
        //Dimensions.removeEventListener('change', this.onDimensionChange);
    }

    render() {
        var { height, width } = Dimensions.get('window');

        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <View style={{ height: 220, margin: 15 }}>
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        alignItems: 'stretch'
                    }}>
                        <Text>Username</Text>
                        <TextInput onChangeText={(text) => this.onChangeText({ username: text})}></TextInput>
                    </View>
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        alignItems: 'stretch'
                    }}>
                        <Text>Password</Text>
                        <TextInput secureTextEntry={true} onChangeText={(text) => this.onChangeText({ password: text})}></TextInput>
                    </View>
                    <View style={{
                        marginTop: 0
                    }}>
                        <Button onPress={this.onLoginPress} title='Login'></Button>
                    </View>
                    <Text>pw: {this.state.password}, username: {this.state.username}</Text>
                    <Text>{this.props.login.receivedAt}, {this.props.login.loginInProgress ? 'true' : 'false'}</Text>
                </View>
            </View>)
    }
}

//Login.propTypes = {
//};

export default connect(state => ({ login: state.login }))(Login)
