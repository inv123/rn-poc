import React from 'react';
import { View, StyleSheet, Image, TouchableWithoutFeedback, TouchableNativeFeedback, Platform, Text, Button, TextInput, Dimensions, ScrollView, Animated, Easing } from 'react-native';
import PropTypes from 'prop-types'
import * as Animatable from 'react-native-animatable';
import { scale, moderateScale, verticalScale } from './../../scaling';

import OptionRating from './option-rating'
import Cross from './../../components/cross'

import styles from './styles'

class OptionItems extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            animateHeight: new Animated.Value(0),
            animateMargin: new Animated.Value(0),
            iconHeight: 0,

            crossColor: '#f53b50',
            crossRoundColor: 'white'
        };

        this.onDimensionChange = this.onDimensionChange.bind(this);
        Dimensions.addEventListener('change', this.onDimensionChange);
    }

    onDimensionChange() {
        // To trigger redraw
        this.setState(Object.assign({}, this.state, {}));
    }

    componentWillUnmount() {
        Dimensions.removeEventListener('change', this.onDimensionChange);
    }

    animated = false;
    _onItemPress() {
        let iconHeight = 0;
        if (!this.animated) {
            Animated.parallel([
                Animated.timing(this.state.animateHeight, {
                    // TODO should use a height based on the item's text
                    toValue: 100,
                    easing: Easing.out(Easing.exp),
                    duration: 500,
                }),
                Animated.timing(this.state.animateMargin, {
                    toValue: 15,
                    easing: Easing.out(Easing.exp),
                    duration: 500,
                })
            ]).start();
            this.animated = true;
            iconHeight = 20;
        } else {
            Animated.parallel([
                Animated.timing(this.state.animateHeight, {
                    toValue: 0,
                    easing: Easing.out(Easing.exp),
                    duration: 500,
                }),
                Animated.timing(this.state.animateMargin, {
                    toValue: 0,
                    easing: Easing.out(Easing.exp),
                    duration: 500,
                }),
            ]).start();
            this.animated = false;
            iconHeight = 0;
        }

        // this.setState(Object.assign({}, this.state, {
        //     iconHeight
        // }));
    }

    _onPlusSignPress() {
        this.props.pressCallback();
        //alert(`pressed ${this.props.name}, price: ${this.props.price}`)
        
        this.setState(Object.assign({}, this.state, {
            crossColor: 'white',
            crossRoundColor: '#f53b50'
        }));


        this.crossView.pulse(250).then(()=> {
            this.setState(Object.assign({}, this.state, {
                crossColor: '#f53b50',
                crossRoundColor: 'white'
            }));
        });
    }

    render() {
        var { height, width } = Dimensions.get('window');

        let paddingLeft = 50;
        // name: 'Fried rice with egg and onion',
        // description: 'The secret to the best Chinise fried rice is onions, garlic and sesame oil, you may add in cooked chicken, beef, pork or shrimp, also you may add in some frozen thawed peas or fresh sauteed or canned mushrooms, whatever you have handy in your fridge!',
        // rating: 4,
        // image: ...
        // weight: '250 g',
        // price: '8.00 $'
        return (
            <View style={{
                flex: 1,
                flexDirection: 'row'
            }}>
                <View style={{
                        width: 20,
                        marginTop: 30
                    }}>
                    <Image style={{
                        width: 70,
                        height: 70,
                        zIndex: 2,
                        transform: [
                            {
                                translateX: 5
                            }
                        ]
                    }} source={{ uri: this.props.image }}></Image>
                </View>

                <View style={{ flex: 80 }}>
                    <TouchableWithoutFeedback onPress={()=> this._onItemPress()}>
                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', zIndex: 1 }}>
                            <View style={StyleSheet.flatten([styles.categorySelectorItem, 
                            {
                                marginRight: 20,
                                marginLeft: 20,
                                zIndex: 2,
                                flex: 10,

                                ...Platform.select({
                                    ios: {
                                        shadowColor: 'black',
                                        shadowOffset: {
                                            width: 0,
                                            height: 2
                                        },
                                        shadowRadius: 5,
                                        shadowOpacity: 0.5
                                    }
                                })
                            }])}>
                                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                    <View style={{ flex: 1 }}>
                                    </View>
                                    <View style={{ flex: 4, paddingTop: 10 }}>
                                        <Text style={{ fontSize: scale(18), paddingRight: 25 }}>{this.props.name}</Text>
                                    </View>
                                </View>
                                
                                <Animated.View style={{ flex: 1, flexDirection: 'row',
                                    marginTop: this.state.animateMargin,
                                    transform:
                                    [
                                        {
                                            translateX: 0
                                        }
                                    ],
                                    height: this.state.animateHeight
                                }}>
                                    <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'space-around', alignItems: 'center', marginTop: 15, marginBottom: 10 }}>
                                        <Image source={require('./../../../img/checkout/ico-hearth.png')}></Image>
                                        <Image source={require('./../../../img/checkout/ico-search.png')}></Image>
                                        <Image source={require('./../../../img/checkout/arrow-icon.png')}></Image>
                                    </View>
                                    <View style={{ flex: 4 }}>
                                        <Text style={{
                                            lineHeight: 20,
                                            paddingRight: 30
                                        }}>{this.props.description}</Text>
                                    </View>
                                </Animated.View>

                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <View style={{ flex: 1 }}>
                                    </View>

                                    <View style={{ flex: 4, flexDirection: 'row', justifyContent: 'space-between' }}>
                                        <View style={{
                                            transform: [
                                                {
                                                    translateY: 15
                                                }
                                            ]
                                        }}>
                                            <OptionRating rating={this.props.rating} outOf={5}></OptionRating>
                                        </View>

                                        <TouchableWithoutFeedback onPress={() => this._onPlusSignPress()}>
                                            <Animatable.View ref={(ins) => this.crossView = ins }>
                                                <View style={{
                                                    borderRadius: 30/2,
                                                    width: 30,
                                                    height: 30,
                                                    backgroundColor: this.state.crossRoundColor,
                                                    elevation: 5,
                                                    //marginLeft: scale(-60),
                                                    margin: 5,
                                                    paddingLeft: 14,
                                                    paddingTop: 8,
                                                    ...Platform.select({
                                                        ios: {
                                                            shadowColor: 'black',
                                                            shadowOffset: {
                                                                width: 0,
                                                                height: 2
                                                            },
                                                            shadowRadius: 2,
                                                            shadowOpacity: 0.5
                                                        }
                                                    })
                                                    }}>
                                                    <Cross color={this.state.crossColor}></Cross>
                                                </View>
                                            </Animatable.View>
                                        </TouchableWithoutFeedback>
                                    </View>
                                </View>

                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <View style={{ flex: 1 }}>
                                    </View>

                                    <View style={{ flex: 4, flexDirection: 'row', justifyContent: 'space-between' }}>
                                        <Text>{this.props.weight}</Text>
                                        <Text style={{ color: '#f53b50', fontWeight: '500', paddingRight: 50 }}>{this.props.price.toFixed(2)} {this.props.data.options.currency}</Text>
                                    </View>
                                </View>
                                {/*
                                <View>
                                </View>
                                */}
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            </View>)
    }
}

export default OptionItems;

OptionItems.propTypes = {
    pressCallback: PropTypes.func
};
