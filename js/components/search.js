import React from 'react';
import { View, StyleSheet, Text, Button, Image, FlatList, TouchableWithoutFeedback, TouchableNativeFeedback, TextInput, Dimensions, ScrollView, Animated, Easing } from 'react-native';
import PropTypes from 'prop-types'

class Search extends React.Component {
    constructor(props) {
        super(props);

        this.state = {

        };

        this.onDimensionChange = this.onDimensionChange.bind(this);
        //Dimensions.addEventListener('change', this.onDimensionChange);
    }

    onDimensionChange() {
        // To trigger redraw
        this.setState(Object.assign({}, this.state, {}));
    }

    componentWillUnmount() {
        //Dimensions.removeEventListener('change', this.onDimensionChange);
    }

    render() {
        var { height, width } = Dimensions.get('window');

        let styles = StyleSheet.create({
            magnifyingGlass: {
            },
            magnifyingGlassCircle: {
              width: 40,
              height: 40,
              borderRadius: 50,
              borderWidth: 5,
              borderColor: this.props.color,
              margin: 5
            },
            magnifyingGlassStick: {
              position: 'absolute',
              right: -30,
              bottom: -12,
              backgroundColor: this.props.color,
              width: 50,
              height: 6,
              transform: [
                { rotate: '45deg' }
              ]
            }
        });

        return (
            <View>
                 <View style={styles.magnifyingGlass}>
                    <View style={styles.magnifyingGlassCircle} />
                    <View style={styles.magnifyingGlassStick} />
                </View>  
            </View>)
    }
}

//CategorySelector.propTypes = {
//};

export default Search;
