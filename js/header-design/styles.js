import { StyleSheet, Dimensions } from 'react-native'
import { scale, moderateScale, verticalScale} from './../scaling';

var { height, width } = Dimensions.get('window');

const styles = {
    header: { 
        height: scale(50)
    },
    title: {
        marginTop: 13,
        // fontWeight: 'bold',
        color: 'black',
        fontSize: scale(22),
        fontFamily: 'cronusRound'
    },
    topLeft: { 
        marginLeft: -10,
        transform: [
            { scaleY: 0.5 },
            { scaleX: 0.5 }
        ]
        // backgroundColor: 'green'
    },
    downChevron: {
        marginTop: 15,
        marginLeft: -15,
        transform: [
            { scaleY: 0.5 },
            { scaleX: 0.5 }
        ]
    },
    search: {
        margin: 10,
        transform: [
            { scaleY: 0.6 },
            { scaleX: 0.6 }
        ]
    },
    itemHeader: {
        //fontWeight: 'bold',
        color: 'black',
        marginBottom: 5,
        fontFamily: 'cronusRound',
        fontSize: scale(20),
    },
    itemImage: {
        padding: 30,
        marginRight: 15,
        //marginTop: 10,
        width: scale(70),
        height: verticalScale(70)
    },
    itemContent: {
        //width: width - 140,
        fontSize: scale(18),
        lineHeight: verticalScale(20),
        fontFamily: 'cronusRound',
    },
    
    droppShadow: { 
        width: width,
        height: verticalScale(450),
        position: 'absolute',
        marginTop: verticalScale(160)
    },

    droppDown: { 
        height: 0, //verticalScale(260),
        width: width,
        position: 'absolute',
        backgroundColor: '#FEFEFE',
        marginTop: verticalScale(59),
        alignSelf: 'stretch',
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,

        // borderRadius: 4,
        // borderWidth: 0.5,
        // borderBottomWidth: 0.5,
        // borderColor: '#d6d7da',
        // borderBottomColor: '#47315a',
        // borderBottomWidth: 15
    },
}

export default StyleSheet.create(styles);