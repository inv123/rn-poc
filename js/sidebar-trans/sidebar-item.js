import React from 'react';
import { View, StyleSheet, Text, Button, Image, FlatList, TouchableWithoutFeedback, TouchableNativeFeedback, TextInput, Dimensions, ScrollView, Animated, Easing } from 'react-native';
import PropTypes from 'prop-types'

class SideBarItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {

        };

        this.onDimensionChange = this.onDimensionChange.bind(this);
        //Dimensions.addEventListener('change', this.onDimensionChange);
    }

    onDimensionChange() {
        // To trigger redraw
        this.setState(Object.assign({}, this.state, {}));
    }

    componentWillUnmount() {
        //Dimensions.removeEventListener('change', this.onDimensionChange);
    }

    render() {
        var { height, width } = Dimensions.get('window');

        return (
            <View style={{
                flexDirection: 'row',
                margin: 5,
                marginTop: 20
            }}>
                <Image style={{ width: 25, height: 25, marginTop: 0, marginRight: 5 }} source={this.props.image}></Image>
                <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{this.props.text.toUpperCase()}</Text>
            </View>)
    }
}

//CategorySelector.propTypes = {
//};

export default SideBarItem;
