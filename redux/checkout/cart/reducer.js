import * as actions from './action'

const initialValue = {
    cart: {
        items: [],
        count: 0,
        summaryData: {
            numOfItems: 0,
            subTotal: 0.00,
            tax: 0.00,
            total: 0.00
        }
    }
}

function getSummaryData(items) {
    let subTotal = 0.00;
    let tax = 0;
    let numOfItems = 0;
    items.forEach(item => {
        numOfItems += item.orderedAmount;
        subTotal += item.price * item.orderedAmount;
        tax += ((item.price * item.taxRate) - item.price) *
            item.orderedAmount;
    });
    
    return {
        numOfItems,
        subTotal: subTotal.toFixed(2),
        tax: tax.toFixed(2),
        total: (subTotal + tax).toFixed(2)
    };
}

function cart(state = initialValue.cart, action) {
    console.log(state, actions)
    switch(action.type) {
        case actions.ALTERQUANTITY:
            // item kikeres, alter quantity, replace the old item
            var foundIndex = state.items.findIndex(x => x.key == action.optionItem.ke);
            let newArray = state.items;
            newArray[foundIndex] = action.optionItem;

            let summaryData = getSummaryData(newArray);

            return Object.assign({}, state, { items: newArray, summaryData });
        break;
        case actions.ADDITEM:
            let oldIndex = state.items.findIndex((x) => x.key == action.optionItem.key);
            if (oldIndex >= 0) {
                // Már fel lett véve az item, mi legyen?
                return state;
            }

            let item = action.optionItem;

            // Default values
            item.orderedAmount = 1;
            item.categoryKey = action.categoryKey;

            var newArray = state.items;
            newArray.push(item);

            var summaryData = getSummaryData(newArray);

            return Object.assign({}, state, { items: newArray, count: state.count + 1, summaryData });
        break;
        case actions.CHECKOUT:
            // TODO dispatch http call

            return Object.assign({}, state, { items: [], count: 0, summaryData: initialValue.cart.summaryData });
        break;

        default:
            return state;
    }
}

export default cart;
