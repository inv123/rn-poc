import React from 'react';
import { View, StyleSheet, Text, Button, TextInput, Dimensions, ScrollView, Animated, Easing, TouchableNativeFeedback, TouchableWithoutFeedback } from 'react-native';
import PropTypes from 'prop-types'

import BurgerChevron from './burger-chevron'
import CartIcon from './cart-icon'

class CheckoutHeader extends React.Component {
    constructor(props) {
        super(props);

        this.state = {

        };

        this.onDimensionChange = this.onDimensionChange.bind(this);
        //Dimensions.addEventListener('change', this.onDimensionChange);
    }

    onDimensionChange() {
        // To trigger redraw
        this.setState(Object.assign({}, this.state, {}));
    }

    componentWillUnmount() {
        //Dimensions.removeEventListener('change', this.onDimensionChange);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.headerStage === this.props.headerStage) return;

        if (nextProps.headerStage == 2 ||
            nextProps.headerStage == 1 && this.props.headerStage == 2) {
                this.cartIconView.triggerChange();
        }
    }

    _onBackPress() {
        if (this.props.headerStage == 0) {
            this.props.navigator.toggleDrawer({
                side: 'left',
                animated: true,
            });
        } else {
            this.props.requestSwitch('prev', null);
        }
    }
    
    _onCheckoutPress() {
        this.props.requestSwitch('next', null);
    }

    render() {
        var { height, width } = Dimensions.get('window');

        return (
            <View style={{ flex: 1,  }}>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <TouchableWithoutFeedback onPress={() => this._onBackPress()}>
                        <View style={{ margin: 8, marginLeft: 28 }}>
                            <BurgerChevron headerStage={this.props.headerStage}></BurgerChevron>
                        </View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress={() => this._onCheckoutPress()}>
                        <View style={{
                            marginRight: 10,
                            height: 35
                        }}>
                            <CartIcon counter={this.props.counter} headerStage={this.props.headerStage} ref={(ins) => this.cartIconView = ins }></CartIcon>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
                {/* <View style={{ flex: 0 }}>
                </View> */}
            </View>)
    }
}

export default CheckoutHeader;

CheckoutHeader.propTypes = {
    requestSwitch: PropTypes.func,
    headerStage: PropTypes.number,
    counter: PropTypes.number
};
