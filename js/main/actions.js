export const INCREMENT = 'main:increment';
export const DECREMENT = 'main:decrement';
export const RESET = 'main:reset';

export function IncrementVar() {
    return { type: INCREMENT };
}
export function DecrementVar() {
    return { type: DECREMENT };
}
export function ResetVarWith(param) {
    return { type: RESET, param };
}
