import React from 'react';
import { View, StyleSheet, Text, Button, TextInput, FlatList, Dimensions, ScrollView, Animated, Easing, TouchableWithoutFeedback, TouchableNativeFeedback } from 'react-native';

import CartItem from './cart-item'
import Chevron from './../../components/chevron'

import * as actions from './../../../redux/checkout/cart/action'

import { scale, verticalScale } from '../../scaling';

class Cart extends React.Component {
    constructor(props) {
        super(props);

        let cartItems = [
            {
                name: 'Fried rice with egg and onion',
                description: 'The secret to the best Chinise fried rice is onions, garlic and sesame oil, you may add in cooked chicken, beef, pork or shrimp, also you may add in some frozen thawed peas or fresh sauteed or canned mushrooms, whatever you have handy in your fridge!',
                rating: 4,
                weight: '250 g',
                price: '8.00 $',
                image: require('./../../../img/checkout/plate-salads.png'),
                orderedAmount: 1,
                key: 1
            },
            {
                name: 'Savannah chopped salad',
                description: 'Bacon ipsum dolor amet meatloaf ball tip bresaola, fatback tongue tail tenderloin rump porchetta beef shankle. Ball tip shoulder ribeye flank short loin biltong frankfurter. Ham hock pig prosciutto, chicken flank sirloin salami turkey.',
                rating: 3,
                weight: '300 g',
                price: '3.00 $',
                image: require('./../../../img/checkout/plate-salad2.png'),
                orderedAmount: 1,
                key: 2
            },
            {
                name: 'Autumn carved Turkey salad',
                description: 'Ham alcatra ribeye porchetta sausage. Pork chop frankfurter rump venison short loin, spare ribs chuck boudin.',
                rating: 4,
                weight: '250 g',
                price: '8.99 $',
                image: require('./../../../img/checkout/plate-salad3.png'),
                orderedAmount: 3,
                key: 3
            }
        ];

        this.state = {
            cartItems,

            ...this.defaultValues()
        };

        this.onDimensionChange = this.onDimensionChange.bind(this);
        Dimensions.addEventListener('change', this.onDimensionChange);
    }

    headerHeight = 55;
    viewVisiblePart = 40;
    additionalAdjustment = 25;
    defaultValues() {
        var { height, width } = Dimensions.get('window');

        return {
            // 55 -> header height
            // 40 -> bottom view's visible part height
            // 25 -> some adjustment this thing needs for whatever reason...
            defaultHeigh: new Animated.Value(height - this.headerHeight - this.viewVisiblePart - verticalScale(this.additionalAdjustment))
        }
    }

    onDimensionChange() {
        // To trigger redraw
        this.setState(Object.assign({}, this.state, {}));
    }

    componentWillUnmount() {
        Dimensions.removeEventListener('change', this.onDimensionChange);
    }

    _onItemPress(item, amount) {
        // Just ignore smaller than zero amouns
        if (amount < 0) return;

        this.updateAmount(item.item.key, amount);
        //alert(JSON.stringify(item))
        //alert(amount)
    }

    updateAmount(key, amount) {
        const item = this.props.cart.items.find((x) => x.key == key);
        item.orderedAmount = amount;

        const { dispatch } = this.props;
        dispatch(actions.AlterQuantity(item));
    }

    _renderCategoryItems(item) {
        return (
            <CartItem pressCallback={(amount) => this._onItemPress(item, amount)} {...item.item} data={this.props.data}></CartItem>
        );
    }

    checked = false;
    _toggleCheckoutView() {
        var { height, width } = Dimensions.get('window');

        if (!this.checked) {
            Animated.parallel([
                Animated.timing(this.state.defaultHeigh, {
                    toValue: height - this.headerHeight - this.viewVisiblePart - verticalScale(this.additionalAdjustment) - 190,
                    easing: Easing.out(Easing.exp),
                    duration: 500,
                }),
                // Gombra katt
                // Animated.timing(animOut, {
                //     toValue: width * -1,
                //     easing: Easing.out(Easing.exp),
                //     duration: 800,
                // })
            ]).start(() => {
                this.checked = true;
            });
        } else {
            Animated.parallel([
                Animated.timing(this.state.defaultHeigh, {
                    toValue: height - this.headerHeight - this.viewVisiblePart - verticalScale(this.additionalAdjustment),
                    easing: Easing.out(Easing.exp),
                    duration: 500,
                }),
                // Gombra katt
                // Animated.timing(animOut, {
                //     toValue: width * -1,
                //     easing: Easing.out(Easing.exp),
                //     duration: 800,
                // })
            ]).start(() => {
                this.checked = false;
            });
        }
        
    }

    _onCheckoutPress() {
        //alert('pressed')

        const { dispatch } = this.props;
        dispatch(actions.Checkout());
    }

    render() {
        var { height, width } = Dimensions.get('window');

        const items = this.props.cart.items.map((object, i) => {
            return this._renderCategoryItems({ item: object });
        });

        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 25 }}>
                    <ScrollView>
                        <Text style={{ fontSize: scale(28), paddingLeft: 40, paddingRight: 120, paddingBottom: 20, backgroundColor: 'transparent' }}>Shopping cart</Text>
                        {items}
                        {/* absolute bottom view height */}
                        <View style={{ height: 55 }}>
                        </View>
                    </ScrollView>
                </View>

                <Animated.View style={{
                    position: 'absolute',
                    width: width,
                    height: 250,
                    borderTopLeftRadius: 45,
                    borderTopRightRadius: 45,
                    backgroundColor: 'white',
                    transform: [
                        {
                            translateY: this.state.defaultHeigh
                        }
                    ]
                }}>
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        marginLeft: 20,
                        marginRight: 40,
                        paddingTop: 12
                    }}>
                        <TouchableWithoutFeedback onPress={() => this._toggleCheckoutView()}>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', }}>
                                <Text style={{ fontWeight: '500' }}>Shopping cart summary</Text>
                                <View style={{
                                    paddingRight: 5,
                                    paddingTop: 5,
                                    transform: [
                                        {
                                            rotate: '-90deg'
                                        },
                                        {
                                            translateX: 4
                                        }
                                    ]
                                }}>
                                    <Chevron color='#f53b50'></Chevron>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                        <View style={{ flex: 5, paddingTop: 15 }}>
                            {/* <View style={{ flex: 1 }}>
                            </View> */}
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={{ fontWeight: '100' }}>Number of items:</Text>
                                <Text style={{ fontWeight: '100' }}>{this.props.cart.summaryData.numOfItems}</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={{ fontWeight: '100' }}>Subtotal:</Text>
                                <Text style={{ fontWeight: '100' }}>{this.props.cart.summaryData.subTotal}</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={{ fontWeight: '100' }}>Tax:</Text>
                                <Text style={{ fontWeight: '100' }}>{this.props.cart.summaryData.tax}</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={{ fontWeight: '100' }}>Total:</Text>
                                <Text style={{ fontWeight: '500', color: '#ff4a62' }}>{this.props.cart.summaryData.total}</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', paddingBottom: 15, marginTop: 15 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontWeight: '100' }}>ICO1</Text>
                                    <Text style={{ fontWeight: '100' }}>ICO2</Text>
                                </View>
                                <TouchableWithoutFeedback onPress={() => this._onCheckoutPress()}>
                                    <View style={{
                                            backgroundColor: '#ff4a62',
                                            borderBottomLeftRadius: 25,
                                            borderTopLeftRadius: 25,
                                            borderTopRightRadius: 25,
                                            borderBottomRightRadius: 25,
                                            marginBottom: 5,
                                            
                                            paddingRight: 20,
                                            paddingLeft: 20,
                                            paddingTop: 4
                                        }}>
                                        
                                        <Text style={{ color: 'white', fontWeight: '100', fontSize: 12 }}>Checkout</Text>
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>
                        </View>
                    </View>
                </Animated.View>
            </View>)
    }
}

export default Cart;
