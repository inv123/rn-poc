import { combineReducers } from 'redux'
import number from './main/reducer'
import plants from './screen-platform/reducer'
import data from './../redux/checkout/data/reducer'
import cart from './../redux/checkout/cart/reducer'
import login from 'appteszt/redux/login/reducer'

// {
//     number: ...,
//     plants: ...
// }
const rootReducer = combineReducers({
    // Plants
    number,
    plants,

    // Checkout
    data,
    cart,

    // Login
    login
})
  
export default rootReducer