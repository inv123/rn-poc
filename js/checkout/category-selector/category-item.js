import React from 'react';
import { View, StyleSheet, Text, Button, Image, TextInput, Platform, Dimensions, ScrollView, Animated, Easing, TouchableWithoutFeedback, TouchableNativeFeedback } from 'react-native';
import PropTypes from 'prop-types'
import * as Animatable from 'react-native-animatable';

import { scale, moderateScale, verticalScale } from './../../scaling';

import Chevron from './../../components/chevron'

import styles from './styles'

var { height, width } = Dimensions.get('window');

class CategoryItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {

        };

        this.onDimensionChange = this.onDimensionChange.bind(this);
        Dimensions.addEventListener('change', this.onDimensionChange);
    }

    onDimensionChange() {
        // To trigger redraw
        this.setState(Object.assign({}, this.state, {}));
    }

    componentWillUnmount() {
        Dimensions.removeEventListener('change', this.onDimensionChange);
    }

    _onItemPress() {
        this.view.pulse(200).then(()=> {
            this.props.pressCallback({
                name: this.props.name,
                subText: this.props.subText
            });
        });
    }

    _onPlusSignPress() {
        alert(`pressed ${this.props.name}, key: ${this.props.key}`);
    }

    render() {
        var { height, width } = Dimensions.get('window');

        return (
            <Animatable.View style={{ flex: 1,
                ...Platform.select({
                    android: {
                        zIndex: 1
                    }
                }),
                flexDirection: 'row', alignItems: 'center' }}
                ref={instance => { this.view = instance; }}>
                <View style={{
                        width: 20,
                        ...Platform.select({
                            ios: {
                                zIndex: 2
                            }
                        })
                    }}>
                    <Image style={{
                        width: 60,
                        height: 60,
                        ...Platform.select({
                            android: {
                                zIndex: 5
                            }
                        }),
                        transform: [
                            {
                                translateX: 20
                            },
                            {
                                translateY: -5
                            }
                        ]
                    }} source={{ uri: this.props.image }}></Image>
                </View>

                <View style={{
                        flex: 80,
                        ...Platform.select({
                            android: {
                                zIndex: 2
                            },
                            ios: {
                                zIndex: 1
                            }
                        })
                    }}>
                    <TouchableWithoutFeedback onPress={()=> this._onItemPress()}>
                        <Animatable.View style={StyleSheet.flatten([styles.categorySelectorItem, 
                        {
                            width: width - 80
                        }])}>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View style={{ flex: 1 }}>
                                    
                                </View>
                                <View style={{ flex: 5, paddingLeft: 5, paddingTop: 5 }}>
                                    <Text style={styles.itemText}>{this.props.name}</Text>
                                    <Text style={styles.itemSubText}>{this.props.subText}</Text>
                                </View>
                            </View>
                        </Animatable.View>
                    </TouchableWithoutFeedback>
                </View>

                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center',
                    ...Platform.select({
                        ios: {
                            zIndex: 2
                        }
                    })
                }}>
                    <View style={{
                        borderRadius: 30/2,
                        width: 30,
                        height: 30,
                        backgroundColor: 'white',
                        elevation: 15,
                        marginLeft: scale(-45),
                        marginBottom: 8,
                        paddingLeft: 10,
                        paddingTop: 5,

                        ...Platform.select({
                            ios: {
                                shadowColor: 'black',
                                shadowOffset: {
                                    width: 0,
                                    height: 2
                                },
                                shadowRadius: 5,
                                shadowOpacity: 0.5
                            }
                        })
                        }}>
                        <Chevron color='#f53b50'></Chevron>
                    </View>
                </View>
            </Animatable.View>
            )
    }
}

export default CategoryItem;

CategoryItem.propTypes = {
    name: PropTypes.string,
    subText: PropTypes.string,
    pressCallback: PropTypes.func
};
