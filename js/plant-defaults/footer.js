import React from 'react';
import { TouchableWithoutFeedback, View, Text, Button, TextInput, Dimensions, StyleSheet, Image } from 'react-native';

import defaultStyles from './../../styles'

class Footer extends React.Component { 
    render() {
        return (
            <View style={defaultStyles.bottomLine}>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>                        
                    <TouchableWithoutFeedback onPress={() => this.props.navBack()}>
                        <Image style={StyleSheet.flatten([defaultStyles.bottomItem, { marginLeft: 20 }])} source={require('./../../img/orangery/tabs/ico_tab1.png')}/>
                    </TouchableWithoutFeedback>
                    <Image style={defaultStyles.bottomItem} source={require('./../../img/orangery/tabs/ico_tab2.png')}/>
                    <Image style={defaultStyles.bottomItem} source={require('./../../img/orangery/tabs/ico_tab3.png')}/>
                    <Image style={StyleSheet.flatten([defaultStyles.bottomItem, { marginRight: 20 }])} source={require('./../../img/orangery/tabs/ico_tab4.png')}/>
                </View>
            </View>
        )
    }
}

export default Footer;