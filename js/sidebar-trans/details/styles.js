import { StyleSheet, Dimensions } from 'react-native'
import { scale, moderateScale, verticalScale} from 'appteszt/js/scaling';

var { height, width } = Dimensions.get('window');

const styles = {
    detailsText: {
        padding: 8,
        marginTop: 8,
        fontSize: 20,
        lineHeight: 30,
        textAlign: 'justify' //ios only :(
    },

    redText: {
        color: '#fb6175'
    },

    normalBottomText: {
        color: '#b5b5b5'
    }
}

export default StyleSheet.create(styles);