import React from 'react';
import { View, StyleSheet, Text, Button, Image, FlatList, Platform, TouchableWithoutFeedback, TouchableNativeFeedback, TextInput, Dimensions, ScrollView, Animated, Easing } from 'react-native';
import PropTypes from 'prop-types'

let CURRENTSTAGE = 0;

class StagedBackground extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			...this.defaultValues()
		};

		this.onDimensionChange = this.onDimensionChange.bind(this);
		Dimensions.addEventListener('change', this.onDimensionChange);
	}

	defaultValues() {
		const { width } = Dimensions.get('window');
		const { views } = this.props;
		let state = {};

		// Move the background view [number of views - 1 / number of views] of the screen to the left.
		const startingTransX = -(views.length - 1) * (width / views.length);
		this.currentPadding = startingTransX;

		for (let index = 0; index < views.length; index++) {
			let defaultWidth;
			if (index == 0) {
				defaultWidth = new Animated.Value(0);
			} else {
				defaultWidth = new Animated.Value(width);
			}

			state[index.toString() + 'id'] = defaultWidth;
		}

		return {
			first: new Animated.Value(startingTransX),

			...state
		};
	}

	onDimensionChange() {
		// To trigger redraw
		this.setState(Object.assign({}, this.state, {}));
	}

	componentWillUnmount() {
		Dimensions.removeEventListener('change', this.onDimensionChange);
	}

	currentPadding = 0
	stageOn(animateParam) {
		const { width } = Dimensions.get('window');
		const { views } = this.props;

		const paddingAmount = (width / views.length);
		this.currentPadding = this.currentPadding + paddingAmount;

		Animated.timing(animateParam, {
			toValue: this.currentPadding,
			easing: Easing.out(Easing.exp),
			duration: 800,
		}).start()
	}

	stageOff(animateParam) {
		const { width } = Dimensions.get('window');
		const { views } = this.props;

		const paddingAmount = (width / views.length);
		this.currentPadding = this.currentPadding - paddingAmount;

		Animated.timing(animateParam, {
			toValue: this.currentPadding,
			easing: Easing.out(Easing.exp),
			duration: 800,
		}).start();
	}

	prev() {
		if (CURRENTSTAGE - 1 < 0) {
			console.warn('No more stages! (prev)');
			return 0;
		} else {
			CURRENTSTAGE--;
		}
		this.stageOff(this.state.first);
	}

	next() {
		const { views } = this.props;
		if (CURRENTSTAGE + 1 >= views.length) {
			console.warn(`No more stages! Max stage is ${views.length}! (next)`);
			return 0;
		} else {
			CURRENTSTAGE++;
		}
		this.stageOn(this.state.first);
	}

	onPrevPressed() {
		this.prev();
	}
	onNextPressed() {
		this.next();
	}

	resetStage() {
		this.setState(Object.assign({}, this.state, this.defaultValues()));
		CURRENTSTAGE = 0;
	}

	componentDidMount() {
		this.resetStage();

		if (this.props.startingStage) {
			if (this.props.startingStage == 1) {
				setTimeout(() => {
					this.next();
				}, 0);
			}
		}
	}

	// props =>
	// 
	render() {
		const { height, width } = Dimensions.get('window');
		const { backgroundColor, views } = this.props;
		var { height, width } = Dimensions.get('window');
		let headerHeight = 45;
		// No idea why i need this here
		// -> android nav width, TODO look at wix navigation, there is a navigation header or something in the docs
		const veirdHeightpad = verticalScale(25);

		viewHeight = height - headerHeight - veirdHeightpad;

		const mappedViews = views.map((Component, index) => {
			return (
				<>
					<Animated.View
						style={{
							position: 'absolute',
							width: width,
							height: viewHeight,
							transform:
								[{
									translateX: this.state[index.toString() + 'id']
								}]
						}}>
						<Component {...childrenProps} />
					</Animated.View>
				</>
			);
		});

		return (
			<View style={{ flex: 1, flexDirection: 'row' }}>
				<View style={{ flex: 1, zIndex: 1 }}>
					<Animated.View style={{
						flex: 1,
						backgroundColor: backgroundColor,
						transform: [
							{
								translateX: this.state.first
							}
						],
					}}>
					</Animated.View>
				</View>

				<View style={{
					position: 'absolute',
					width: width,
					height: height,
					zIndex: 4,
					...Platform.select({
						ios: {
							marginTop: 30
						}
					})
				}}>
					{/* Header bar */}
					{this.props.children}

					<View style={{
						flex: 15, flexDirection: 'row',
						marginTop: headerHeight
					}}>
						{mappedViews}
					</View>
					{/* <Button title='Animate' onPress={() => this.onNextPressed()}></Button>
                    <Button onPress={()=> this.onPrevPressed()} title="Prev"></Button> */}
				</View>
			</View>)
	}
}

//CategorySelector.propTypes = {
//};

export default StagedBackground;
