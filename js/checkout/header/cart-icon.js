import React from 'react';
import { View, StyleSheet, Text, Button, TouchableWithoutFeedback, TouchableNativeFeedback, TextInput, Dimensions, ScrollView, Animated, Easing } from 'react-native';
import PropTypes from 'prop-types'
import { scale } from '../../scaling';

class CartIcon extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            textColor: 'white',
            outlineColor: '#f53b50'
        };

        this.onDimensionChange = this.onDimensionChange.bind(this);
        Dimensions.addEventListener('change', this.onDimensionChange);
    }

    onDimensionChange() {
        // To trigger redraw
        //this.setState(Object.assign({}, this.state, {}));
        this.setState({
            textColor: 'white',
            outlineColor: '#f53b50'
        });
    }

    componentWillUnmount() {
        Dimensions.removeEventListener('change', this.onDimensionChange);
    }

    stage = 0;
    triggerChange() {
        if (this.stage == 0) {
            this.setState({
                textColor: '#f53b50',
                outlineColor: 'white'
            });
            this.stage = 1;
        } else {
            this.setState({
                textColor: 'white',
                outlineColor: '#f53b50'
            });
            this.stage = 0;
        }
    }

    render() {
        var { height, width } = Dimensions.get('window');
        //alert(this.state.outlineColor)

        return (
            <View style={{
                flex: 1,
                marginTop: 4,
                marginRight: 10
            }}>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={{
                        //flex: 1,
                        width: 10,
                        height: 1.5,
                        backgroundColor: this.state.outlineColor,
                        transform: [
                            {
                                translateY: 14
                            },
                            {
                                rotate: '-60deg'
                            }
                        ]
                    }}>
                    </View>
                    <View style={{
                        width: 10,
                        height: 1.5,
                        backgroundColor: this.state.outlineColor,
                        transform: [
                            {
                                translateY: 14
                            },
                            {
                                rotate: '60deg'
                            }
                        ]
                    }}>
                    </View>
                </View>
                <View style={{
                        width: 15,
                        height: 15,
                        borderRadius: 15/2,
                        backgroundColor: this.state.outlineColor,
                        transform: [
                            {
                                translateX: 10
                            },
                            {
                                translateY: 2
                            }
                        ]
                    }}>
                        <Text style={{ fontSize: 11, color: this.state.textColor, marginLeft: 4 }}>{this.props.counter}</Text>
                    </View>
                <View style={{
                    flex: 1,
                    width: 20,
                    height: 0,
                    borderBottomWidth: 10,
                    borderBottomColor: this.state.outlineColor,
                    borderLeftWidth: 4,
                    borderLeftColor: 'transparent',
                    borderRightWidth: 4,
                    borderRightColor: 'transparent',
                    borderStyle: 'solid',
                    transform: [
                        {
                            rotate: '180deg'
                        }
                    ]
                }}>
                </View>
            </View>)
    }
}

CartIcon.propTypes = {
    //triggerSwitch: PropTypes.func
    counter: PropTypes.number
};

export default CartIcon;
