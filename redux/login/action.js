export const LOGINREQUESTOK = 'loginRequestResponse'
export const REQUESTSTATUSCHANGED = 'requestStatusChanged'

export function LoginRequestOk(json) {
    return { type: LOGINREQUESTOK, receivedAt: Date.now(), responseData: json };
}
function changeStatus(loginInProgress) {
    return { type: REQUESTSTATUSCHANGED, loginInProgress, receivedAt: Date.now() };
}

function fetchPosts() {
    return dispatch => {
        return fetch('https://myapi.com/postme', {
            method: 'POST',
            headers: {
                // TODO token ?
            }
        })
        .then(resp => resp.json())
        .then(json => {
            console.log(json);
            dispatch(LoginRequestOk(json.data));
        })
        .catch(error => {
            dispatch(changeStatus(false));
            console.error(error);
        });
    }
}

function shouldFetch(state) {
    if (state.isFetching) {
        return false;
    } else {
        return true;
    }
}

export function fetchData() {
    return (dispatch, getState) => {
        // Notice about the api call
        dispatch(changeStatus(true));

        if (shouldFetch(getState())) {
            return dispatch(fetchPosts());
        } else {
            return Promise.resolve();
        }
    }
}
