export const RECEIVEDDATA = 'receivedData'
export const FETCHDATA = 'fetchData'
export const RECEIVEDOPTIONS = 'receivedOptions'
export const SELECTEDCATEGORY = 'selectedCategory'

export function ReceivedData(json) {
    return { type: RECEIVEDDATA, receivedAt: Date.now(), items: json };
}
function changeStatus(isFetching) {
    return { type: FETCHDATA, isFetching };
}
function receivedOptions(options) {
    return { type: RECEIVEDOPTIONS, options };
}
export function selectedCategory(category, categoryName, categoryKey) {
    return { type: SELECTEDCATEGORY, category, categoryName, categoryKey };
}


function fetchPosts() {
    return dispatch => {
        return fetch('https://gergo-aws-teszt.s3.amazonaws.com/data/checkout-data.js')
        .then(resp => resp.json())
        .then(json => {
            console.log(json);
            dispatch(ReceivedData(json.data));
            dispatch(receivedOptions(json.options));
        })
        .catch(error => {
            dispatch(changeStatus(false));
            console.error(error);
        });
    }
}

function shouldFetch(state) {
    if (state.isFetching) {
        return false;
    } else {
        return true;
    }
}

export function fetchData() {
    return (dispatch, getState) => {
        // Notice about the api call
        dispatch(changeStatus(true));

        if (shouldFetch(getState())) {
            return dispatch(fetchPosts());
        } else {
            return Promise.resolve();
        }
    }
}
