export const RECEIVEDPLANTS = 'receivedplants'
export const FETCHPLANTS = 'fetchplants'

export function ReceivedPlants(json) {
    return { type: RECEIVEDPLANTS, receivedAt: Date.now(), items: json };
}
function changeStatus(isFetching) {
    return { type: FETCHPLANTS, isFetching: isFetching };
}


function fetchPosts() {
    return dispatch => {
        return fetch('https://api.myjson.com/bins/mj0kz') 
        .then(resp => resp.json())
        .then(json => {
            dispatch(ReceivedPlants(json.plants));
        })
        .catch(error => {
            dispatch(changeStatus(false));
            //console.error(error);
        });
    }
}

function shouldFetch(state) {
    if (state.isFetching) {
        return false;
    } else {
        return true;
    }
}

export function fetchPlants() {
    return (dispatch, getState) => {
        // Notice about the api call
        dispatch(changeStatus(true));

        if (shouldFetch(getState())) {
            return dispatch(fetchPosts());
        } else {
            return Promise.resolve();
        }
    }
}
