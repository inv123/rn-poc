package com.appteszt;

import android.view.LayoutInflater;
import android.view.View;

import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;

/**
 * Created by GergoN on 2017.12.06..
 */

public class ShadowDroppViewManager extends SimpleViewManager<View> {
    @Override
    public String getName() {
        return "ShadowDroppView";
    }

    @Override
    protected View createViewInstance(ThemedReactContext reactContext) {
        return LayoutInflater.from(reactContext).inflate(R.layout.layout, null);
    }
}
