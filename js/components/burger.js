import React from 'react';
import { View, StyleSheet, Text, Button, Image, FlatList, TouchableWithoutFeedback, TouchableNativeFeedback, TextInput, Dimensions, ScrollView, Animated, Easing } from 'react-native';
import PropTypes from 'prop-types'

class Burger extends React.Component {
    constructor(props) {
        super(props);

        this.state = {

        };

        this.onDimensionChange = this.onDimensionChange.bind(this);
        //Dimensions.addEventListener('change', this.onDimensionChange);
    }

    onDimensionChange() {
        // To trigger redraw
        this.setState(Object.assign({}, this.state, {}));
    }

    componentWillUnmount() {
        //Dimensions.removeEventListener('change', this.onDimensionChange);
    }

    render() {
        var { height, width } = Dimensions.get('window');

        return (
            <View style={{ width: 50, height: 55 }}>    
                <View style={{ flex: 1, justifyContent: 'center', paddingLeft: 8 }}>
                    <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'space-between' }}>
                        <View style={{ width: 35, height: 4, backgroundColor: this.props.color, marginTop: 16 }}>
                        </View>
                        <View style={{ width: 20, height: 4, backgroundColor: this.props.color }}>
                        </View>
                        <View style={{ width: 30, height: 4, backgroundColor: this.props.color, marginBottom: 16 }}>
                        </View>
                    </View>
                </View>
            </View>)
    }
}

//CategorySelector.propTypes = {
//};

export default Burger;
