import { StyleSheet, Dimensions } from 'react-native'
import { scale, moderateScale, verticalScale } from './js/scaling';

var { height, width } = Dimensions.get('window');

// Ios and android differences
import style from './style'

const baseStyles = {
    header: { 
        height: scale(50)
    },
    title: {
        marginTop: 13,
        // fontWeight: 'bold',
        color: 'black',
        fontSize: scale(22),
        fontFamily: 'cronusRound'
    },
    topLeft: { 
        marginLeft: -10,
        transform: [
            { scaleY: 0.5 },
            { scaleX: 0.5 }
        ]
        // backgroundColor: 'green'
    },
    downChevron: {
        marginTop: 15,
        marginLeft: -15,
        transform: [
            { scaleY: 0.5 },
            { scaleX: 0.5 }
        ]
    },
    search: {
        margin: 10,
        transform: [
            { scaleY: 0.6 },
            { scaleX: 0.6 }
        ]
    },
    droppShadow: { 
        width: width,
        height: verticalScale(450),
        position: 'absolute',
        marginTop: verticalScale(160)
    },
    droppDown: { 
        height: verticalScale(260),
        width: width,
        position: 'absolute',
        backgroundColor: '#FEFEFE',
        marginTop: verticalScale(59),
        alignSelf: 'stretch',
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,

        // borderRadius: 4,
        // borderWidth: 0.5,
        // borderBottomWidth: 0.5,
        // borderColor: '#d6d7da',
        // borderBottomColor: '#47315a',
        // borderBottomWidth: 15
    },

    bottomLine: {
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        height: verticalScale(50),
        backgroundColor: '#8FD9A2'
    },
    bottomItem: {
        width: scale(27),
        height: verticalScale(28),
        marginTop: verticalScale(10)
    },
}

const styles = Object.assign({}, baseStyles, style);

//console.log(styles);

export default StyleSheet.create(styles);