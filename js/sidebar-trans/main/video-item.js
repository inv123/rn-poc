import React from 'react';
import { View, StyleSheet, Text, Button, Image, FlatList, TouchableWithoutFeedback, TouchableNativeFeedback, TextInput, Dimensions, ScrollView, Animated, Easing } from 'react-native';
import PropTypes from 'prop-types'

import { scale, verticalScale } from 'appteszt/js/scaling';

import styles from './styles'

class VideoItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {

        };

        this.onDimensionChange = this.onDimensionChange.bind(this);
        //Dimensions.addEventListener('change', this.onDimensionChange);
    }

    onDimensionChange() {
        // To trigger redraw
        this.setState(Object.assign({}, this.state, {}));
    }

    componentWillUnmount() {
        //Dimensions.removeEventListener('change', this.onDimensionChange);
    }

    render() {
        var { height, width } = Dimensions.get('window');

        // const videoItem = {
        //     text: 'Comment on the importance of human life',
        //     type: 'Video',
        //     commentCount: 45,
        //     image: require('appteszt/img/sidebar/img4.png')
        // };

        return (
            <View style={{ height: verticalScale(160), marginRight: 5 }}>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end' }}>
                    <View style={{ width: 160 }}>
                        <TouchableNativeFeedback onPress={() => this.props.changeView('details')}>
                            <Image style={{
                                flex: 1,
                                width: scale(140),
                                height: null,
                                resizeMode: 'contain',
                                margin: 5,
                                marginLeft: 6
                            }} source={this.props.image}></Image>
                        </TouchableNativeFeedback>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'space-between', marginLeft: 2 }}>
                        <View style={{ flex: 1 }}>
                            <Text style={styles.titleText}>{this.props.text}</Text>
                        </View>
                        <View style={{
                                flex: 1,
                                flexDirection: 'row',
                                alignItems: 'flex-end',
                                marginBottom: 8,
                                justifyContent: 'space-between'
                            }}>
                            <View style={{ flex: 1 }}>
                                <Text style={styles.typeText}>{this.props.type.toUpperCase()}</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end' }}>
                                <Image style={{ marginTop: 0, transform: [{ scale: 0.8 }] }} source={require('appteszt/img/sidebar/ico_text.png')}></Image>
                                <Text style={{ fontSize: 12 }}>{this.props.commentCount}</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>)
    }
}

//CategorySelector.propTypes = {
//};

export default VideoItem;
