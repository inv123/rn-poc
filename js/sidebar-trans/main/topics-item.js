import React from 'react';
import { View, StyleSheet, Text, Button, Image, FlatList, TouchableWithoutFeedback, TouchableNativeFeedback, TextInput, Dimensions, ScrollView, Animated, Easing } from 'react-native';
import PropTypes from 'prop-types'

import { scale, verticalScale } from 'appteszt/js/scaling';

class TopicsItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {

        };

        this.onDimensionChange = this.onDimensionChange.bind(this);
        //Dimensions.addEventListener('change', this.onDimensionChange);
    }

    onDimensionChange() {
        // To trigger redraw
        this.setState(Object.assign({}, this.state, {}));
    }

    componentWillUnmount() {
        //Dimensions.removeEventListener('change', this.onDimensionChange);
    }

    render() {
        var { height, width } = Dimensions.get('window');

        var activeStyle = (active) =>{
            if (active) {
                return 6;
            } else {
                return 0;
            }
        }

        return (
            <View style={{
                    marginTop: 5,
                    marginLeft: 10,
                    marginBottom: 5,
                    padding: 10,
                    paddingTop: 10,
                    paddingLeft: 15,
                    paddingRight: 15,

                    borderBottomLeftRadius: 5,
                    borderBottomRightRadius: 5,
                    borderTopLeftRadius: 5,
                    borderTopRightRadius: 5,

                    backgroundColor: 'white',
                    elevation: activeStyle(this.props.active)
                    
                }}>
                <Text style={{ fontSize: scale(18), color: 'black' }}>{this.props.text}</Text>
            </View>)
    }
}

//CategorySelector.propTypes = {
//};

export default TopicsItem;
