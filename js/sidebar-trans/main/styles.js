import { StyleSheet, Dimensions } from 'react-native'
import { scale, moderateScale, verticalScale} from 'appteszt/js/scaling';

var { height, width } = Dimensions.get('window');

const styles = {
    image: {
        // flex: 1,
        // width: 280,
        // height: scale(160),
        flex: 1,
        width: null,
        height: null,
        resizeMode: 'contain',

        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,

        // margin: 2
    },
    fRowImgView: {
        marginLeft: 5,
        //elevation: 6,
        width: scale(330),
        height: verticalScale(160),
        //marginRight: 8,
        //paddingTop: 2,
        //marginTop: -25,
        transform:[
            {
                scale: 1
            }
        ]
        //backgroundColor: 'red'

        // borderBottomLeftRadius: 5,
        // borderBottomRightRadius: 5,
        // borderTopLeftRadius: 5,
        // borderTopRightRadius: 5,
    },
    typeText: { fontSize: 14, color: '#d0d0d0' },

    titleText: {
        fontSize: 20, color: '#666666', fontWeight: 'bold'
    },
    tsImage: {
        flex: 1,
        width: scale(120),
        height: null,
        resizeMode: 'contain',
        margin: 0,
        margin: 5
    }
}

export default StyleSheet.create(styles);