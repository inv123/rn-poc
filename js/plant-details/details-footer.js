import React from 'react';
import { View, Text, Button, TextInput, Dimensions, StyleSheet, Image } from 'react-native';
import { scale, moderateScale, verticalScale} from './../scaling';

import styles from './styles'

class DetailsFooter extends React.Component { 
    render() {
        return (
            <View style={styles.bottomLine}>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end' }}>
                    <Text style={StyleSheet.flatten([styles.bottomText, { flex: 5, marginLeft: 20, marginBottom: 4 }])}>
                        Check in the market
                    </Text>
                    <View style={styles.bottomRightCorner}>
                        <Text style={styles.bottomText}>$10</Text>
                    </View>
                </View>
            </View>
        )
    }
}

export default DetailsFooter;