import React from 'react';
import { View, StyleSheet, Image, TouchableWithoutFeedback, Platform, TouchableNativeFeedback, Text, Button, TextInput, Dimensions, ScrollView, Animated, Easing } from 'react-native';
import PropTypes from 'prop-types'
import * as Animatable from 'react-native-animatable';
import { scale, moderateScale, verticalScale } from './../../scaling';

import OptionRating from './../option-list/option-rating'
import Cross from './../../components/cross'

import styles from './styles'

class CartItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            animateHeight: new Animated.Value(0),
            animateMargin: new Animated.Value(0),

            crossColor: '#f53b50',
            crossRoundColor: 'white'
        };

        this.onDimensionChange = this.onDimensionChange.bind(this);
        Dimensions.addEventListener('change', this.onDimensionChange);
    }

    onDimensionChange() {
        // To trigger redraw
        this.setState(Object.assign({}, this.state, {}));
    }

    componentWillUnmount() {
        Dimensions.removeEventListener('change', this.onDimensionChange);
    }

    render() {
        var { height, width } = Dimensions.get('window');

        let paddingLeft = 50;
        // name: 'Fried rice with egg and onion',
        // description: 'The secret to the best Chinise fried rice is onions, garlic and sesame oil, you may add in cooked chicken, beef, pork or shrimp, also you may add in some frozen thawed peas or fresh sauteed or canned mushrooms, whatever you have handy in your fridge!',
        // rating: 4,
        // image: ...
        // weight: '250 g',
        // price: '8.00 $'
        return (
            <View style={{
                flex: 1,
                flexDirection: 'row'
            }}>
                <View style={{
                        width: 20,
                        marginTop: 25,
                        ...Platform.select({
                            ios: {
                                zIndex: 2
                            }
                        })
                    }}>
                    <Image style={{
                        width: 70,
                        height: 70,
                        ...Platform.select({
                            android: {
                                zIndex: 5
                            }
                        }),
                        transform: [
                            {
                                translateX: 5
                            }
                        ]
                    }} source={{ uri: this.props.image }}></Image>
                </View>

                <View style={{ flex: 80 }}>
                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', zIndex: 1 }}>
                        <View style={StyleSheet.flatten([styles.categorySelectorItem, 
                        {
                            marginRight: 20,
                            marginLeft: 20,
                            flex: 1
                        }])}>
                            <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                <View style={{ flex: 1 }}>
                                </View>
                                <View style={{ flex: 4, paddingTop: 10, paddingRight: 20, paddingLeft: 5 }}>
                                    <Text>{this.props.name}</Text>
                                </View>
                            </View>

                            <View style={{ flex: 1, flexDirection: 'row', marginTop: 10 }}>
                                <View style={{ flex: 1 }}>
                                </View>
                                <View style={{ flex: 4, flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <View style={{
                                        transform: [
                                            {
                                                translateY: 0
                                            }
                                        ]
                                    }}>
                                        <OptionRating rating={this.props.rating} outOf={5}></OptionRating>
                                    </View>
                                </View>
                            </View>

                            <View style={{ flex: 1, flexDirection: 'row', marginTop: 5 }}>
                                <View style={{ flex: 1 }}>
                                </View>

                                <View style={{ flex: 4, flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text>{this.props.weight}</Text>
                                    <Text style={{ color: '#f53b50', fontWeight: '500', paddingRight: 50 }}>{this.props.price.toFixed(2)} {this.props.data.options.currency}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>

                <View style={{
                        width: 20,
                        marginTop: 15
                    }}>
                    <View style={{
                        width: 30,
                        zIndex: 5,
                        backgroundColor: 'white',
                        transform: [
                            {
                                translateX: -35
                            }
                        ],
                        borderBottomLeftRadius: 20,
                        borderBottomRightRadius:20,
                        borderTopLeftRadius: 20,
                        borderTopRightRadius: 20,
                        marginBottom: 25,
                        
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'space-between',
                        elevation: 5,

                        ...Platform.select({
                            ios: {
                                shadowColor: 'black',
                                shadowOffset: {
                                    width: 0,
                                    height: 1
                                },
                                shadowRadius: 3,
                                shadowOpacity: 0.5
                            }
                        })
                    }}>
                        <TouchableWithoutFeedback onPress={() => this.props.pressCallback(this.props.orderedAmount - 1)}>
                            <View style={{ paddingTop: 0, paddingBottom: 10 }}>
                                <View style={{
                                    backgroundColor: '#808080',
                                    width: 15,
                                    height: 2,
                                    marginTop: 15,
                                    marginLeft: 8
                                }}>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>

                        <View style={{ marginLeft: 12, marginTop: 5 }}>
                            <Text>{this.props.orderedAmount}</Text>
                        </View>

                        <TouchableWithoutFeedback onPress={() => this.props.pressCallback(this.props.orderedAmount + 1)}>
                            <View style={{
                                flex: 1,
                                marginTop: 15,
                                marginLeft: 7,
                                paddingLeft: 8
                            }}>
                                <Cross color='#f53b50'></Cross>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </View>
            </View>)
    }
}

export default CartItem;

CartItem.propTypes = {
    pressCallback: PropTypes.func
};
