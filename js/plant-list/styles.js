import { StyleSheet, Dimensions } from 'react-native'
import { scale, moderateScale, verticalScale} from './../scaling';

var { height, width } = Dimensions.get('window');

const styles = {
    scrollViewAlphabet: {
        
        margin: 5,
        padding: 15,
        paddingTop: 10,
        marginBottom: 15,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5
    },
    activeItemAlphabetItem: {
        backgroundColor: '#D7E1EA',  
    },

    item: {
        margin: 10,
        fontSize: scale(18),
        //lineHeight: verticalScale(20),
        fontFamily: 'cronusRound',
        marginLeft: 15
    },
    

}

export default StyleSheet.create(styles);