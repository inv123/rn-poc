import { StyleSheet, Dimensions, Platform } from 'react-native'
import { scale, moderateScale, verticalScale } from './../../scaling';

var { height, width } = Dimensions.get('window');

const styles = {
    categorySelectorItem: {
        margin: 5,
        //padding: 0,
        paddingTop: 15,
        paddingBottom: 15,

        marginBottom: 15,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        
        alignSelf: 'center',
        backgroundColor: 'white',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',

        elevation: 8,
        
        ...Platform.select({
            ios: {
                shadowColor: 'black',
                shadowOffset: {
                    width: 0,
                    height: 1
                },
                shadowRadius: 5,
                shadowOpacity: 0.5
            }
        })
    },
    itemText: {
        fontSize: scale(24),
        paddingRight: 50,
    },
    itemSubText: {
        fontSize: scale(12)
    }

}

export default StyleSheet.create(styles);