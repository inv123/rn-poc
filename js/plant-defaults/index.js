import React from 'react';
import { View, Text, Button, TextInput, Dimensions, ScrollView } from 'react-native';
import Header from './header'
import DropShadowView from './../native/drop-shadow'
import DropDownItem from './drop-down-item'
import Footer from './footer'

//import styles from './styles'
import defaultStyles from './../../styles'

class PlantDefaults extends React.Component {

    static navigatorStyle = {
        drawUnderNavBar: true,
        navBarTranslucent: true,
        navBarHidden: true
    };

    constructor(props) {
        super(props);

        let dropDownOptions = [];
        dropDownOptions.push({ key: 0, title: 'Catalogue', icon: require('./../../img/orangery/tools.png') });
        dropDownOptions.push({ key: 1, title: 'Inspirations', icon: require('./../../img/orangery/recipes.png') });
        dropDownOptions.push({ key: 2, title: 'Tips', icon: require('./../../img/orangery/tools.png') });
        dropDownOptions.push({ key: 3, title: 'Recipes', icon: require('./../../img/orangery/recipes.png') });

        this.state = {
            dropDownOptions: dropDownOptions,
            dropDownVisible: false
        }
    }

    onDroppDownClick() {
        this.setState(Object.assign({}, this.state, {
            dropDownVisible: !this.state.dropDownVisible
        }))
    }

    render() {
        var dropDownOptions = this.state.dropDownOptions.map(function(object, i) {
            return <DropDownItem {...object}></DropDownItem>;
        });

        return (
            <View style={{ flex: 1, flexDirection: 'column', backgroundColor: '#FEFEFE' }}>
                <Header droppDownClickHandler={() => this.onDroppDownClick()} />

                <View style={{ flex: 1 }}>
                    <Text>Some shitty content goes here</Text>
                </View>

                { this.state.dropDownVisible ? 
                    <DropShadowView style={defaultStyles.droppShadow} />
                : null }

                { this.state.dropDownVisible ? 
                    <View style={defaultStyles.droppDown}>
                        {dropDownOptions}
                    </View>
                : null }

                <Footer />
            </View>
        )
    }
}

export default PlantDefaults;