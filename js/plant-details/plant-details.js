import React from 'react';
import { View, Text, Button, TextInput, Dimensions, ScrollView, Image } from 'react-native';
import Header from './../plant-defaults/header'
import DropShadowView from './../native/drop-shadow'
import DropDownItem from './../plant-defaults/drop-down-item'
import DetailsFooter from './details-footer'
import PlantProgressBar from './../components/plant-progress-bar/plant-progress-bar'

import { scale, moderateScale, verticalScale } from './../scaling';
import defaultStyles from './../../styles'
import styles from './styles'

class PlantDetails extends React.Component {

    static navigatorStyle = {
        drawUnderNavBar: true,
        navBarTranslucent: true,
        navBarHidden: true
    };

    constructor(props) {
        super(props);

        let dropDownOptions = [];
        dropDownOptions.push({ key: 0, title: 'Catalogue', icon: require('./../../img/orangery/tools.png') });
        dropDownOptions.push({ key: 1, title: 'Inspirations', icon: require('./../../img/orangery/recipes.png') });
        dropDownOptions.push({ key: 2, title: 'Tips', icon: require('./../../img/orangery/tools.png') });
        dropDownOptions.push({ key: 3, title: 'Recipes', icon: require('./../../img/orangery/recipes.png') });

        this.state = {
            dropDownOptions: dropDownOptions,
            dropDownVisible: false
        }
    }

    onDroppDownClick() {
        this.setState(Object.assign({}, this.state, {
            dropDownVisible: !this.state.dropDownVisible
        }))
    }

    render() {
        var dropDownOptions = this.state.dropDownOptions.map(function(object, i) {
            return <DropDownItem {...object}></DropDownItem>;
        });

        return (
            <View style={{ flex: 1, flexDirection: 'column', backgroundColor: '#FEFEFE' }}>
                <View style={styles.header}>
                    <View style={{
                            flex: 1,
                            flexDirection: 'row',
                            justifyContent: 'space-between'
                        }}>
                        <Image style={styles.topLeft} source={require('./../../img/orangery/ico_left_green.png')}/>
                        <Text style={styles.middleText}>Ferocactus</Text>
                        <Image style={styles.topRight} source={require('./../../img/orangery/ico_plus.png')}/>
                    </View>
                </View>
                
                {/* <Header droppDownClickHandler={() => this.onDroppDownClick()} /> */}

                <View style={{ flex: 1 }}>
                    <ScrollView>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <Image style={styles.plantImage} source={require('./../../img/orangery/details_cactus.png')} />
                            <View style={{ flex: 1}}>
                            </View>
                            <View style={{ flex: 2, alignItems: 'flex-start', marginTop: 70, marginRight: 50 }}>
                                <Text style={styles.descLeft}>Kingdom:</Text>
                                <Text style={styles.descLeft}>Family:</Text>
                                <Text style={styles.descLeft}>Subfamily:</Text>
                                <Text style={styles.descLeft}>Tribe:</Text>
                            </View>
                            <View style={{ flex: 3, alignItems: 'flex-start', marginTop: 70, marginRight: 5 }}>
                                <Text style={styles.descRight}>Plantoe</Text>
                                <Text style={styles.descRight}>Cactaceae</Text>
                                <Text style={styles.descRight}>Cactaoideae</Text>
                                <Text style={styles.descRight}>Cacteae</Text>
                            </View>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'column', margin: 5 }}>
                            <Text style={styles.cultivation}>CULTIVATION</Text>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View style={{ flex: 2 }}>
                                </View>
                                <View style={{ flex: 4 }}>
                                    <Text style={styles.cultivationProgress}>Water</Text>
                                    <Text style={styles.cultivationProgress}>Drainage</Text>
                                    <Text style={styles.cultivationProgress}>Sun</Text>
                                </View>

                                <View style={{ flex: 2 }}>
                                    <Image style={{ transform: [{ scaleY: 0.7 }, { scaleX: 0.7 }] }}
                                            source={require('./../../img/orangery/ico_drop.png')} />
                                    <View style={{
                                        height: 10,
                                        width: 10,
                                        marginLeft: 3,
                                        marginTop: 7,
                                        backgroundColor: '#e8bb8d',
                                        borderBottomLeftRadius: 2,
                                        borderBottomRightRadius: 2,
                                        borderTopLeftRadius: 2,
                                        borderTopRightRadius: 2
                                     }}>
                                    </View>
                                    <View style={{
                                        height: 10,
                                        width: 10,
                                        padding: 5,
                                        marginLeft: 3,
                                        marginTop: 12,
                                        backgroundColor: '#e7d25d',
                                        borderBottomLeftRadius: 10,
                                        borderBottomRightRadius: 10,
                                        borderTopLeftRadius: 10,
                                        borderTopRightRadius: 10
                                     }}>
                                    </View>
                                </View>
                                
                                <View style={{ flex: 8 }}>
                                    <PlantProgressBar baseLineColor={'#90acec'} baseLineOffColor={'#d8e3ff'} progress={25} />
                                    <PlantProgressBar baseLineColor={'#e8bb8d'} baseLineOffColor={'#faece1'} progress={75} />
                                    <PlantProgressBar baseLineColor={'#e7d25d'} baseLineOffColor={'#faf3df'} progress={100} />
                                </View>
                            </View>
                            
                            <Text style={styles.description}>Bacon ipsum dolor amet beef t-bone pork belly, flank leberkas landjaeger cow pork tail brisket. Biltong pancetta jerky fatback, bresaola rump doner venison boudin. Tail andouille shoulder spare ribs venison burgdoggen filet mignon turducken sirloin chuck. Beef ribs strip steak rump bresaola tail flank leberkas pancetta turkey cupim doner.</Text>
                        </View>
                    </ScrollView>
                </View>

                { this.state.dropDownVisible ? 
                    <DropShadowView style={defaultStyles.droppShadow} />
                : null }

                { this.state.dropDownVisible ? 
                    <View style={defaultStyles.droppDown}>
                        {dropDownOptions}
                    </View>
                : null }

                {/* <DropShadowView style={styles.descShadow} /> */}

                <DetailsFooter />
            </View>
        )
    }
}

export default PlantDetails;