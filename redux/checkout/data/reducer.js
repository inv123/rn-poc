import * as actions from './action'

const initialValue = {
    data: {
        isFetching: false,
        receivedAt: Date.now(),
        items: [],
        options: {
            currency: '$'
        },

        categoryOptions: [

        ],
        // `$1 menu`
        selectedCategory:  {
            name: '',
            key: -1,
        }
    }
}

function data(state = initialValue.data, action) {
    //console.log(state, actions)
    switch(action.type) {
        case actions.FETCHDATA:
            console.log('fetchdata');
            return Object.assign({}, state, { isFetching: action.isFetching });
        break;
        case actions.RECEIVEDDATA:
            console.log('receiveddata');
            return Object.assign({}, state, { items: action.items.categories, receivedAt: action.receivedAt, isFetching: false });
        break;
        case actions.RECEIVEDOPTIONS:
            console.log('receivedoptions');
            return Object.assign({}, state, { options: action.options });
        break;
        case actions.SELECTEDCATEGORY:
            console.log('selectedCategory');
            return Object.assign({}, state, { categoryOptions: action.category, selectedCategory: { name: action.categoryName, key: action.categoryKey } });
        break;
        default:
            return state;
    }
}

export default data;
