import React from 'react';
import { View, Text, Button, TextInput, Dimensions, TouchableHighlight, Image } from 'react-native';
import PropTypes from 'prop-types';

import defaultStyle from './../../styles'

class Header extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            dropDownVisible: false
        };
    }

    render() {
        return (
            <View style={defaultStyle.header}>
                <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'space-between'
                    }}>
                    <TouchableHighlight onPress={()=> this.props.droppDownClickHandler() }>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <Image style={defaultStyle.topLeft} source={require('./../../img/orangery/topleft.png')}/>
                            <Image style={defaultStyle.downChevron} source={require('./../../img/down-chevron.png')}/>
                        </View>
                    </TouchableHighlight>
                    <Text style={defaultStyle.title}>The Big Book of Plants</Text>
                    <Image style={defaultStyle.search} source={require('./../../img/orangery/topright.png')}/>
                </View>
            </View>
        )
    }
}

Header.propTypes = {
    // the StyledButton requires a clickHandler
    //droppDownClickHandler: PropTypes.func.require,
}

export default Header;