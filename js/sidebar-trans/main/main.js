import React from 'react';
import { View, StyleSheet, Text, Button, Image, FlatList, TouchableWithoutFeedback, TouchableNativeFeedback, TextInput, Dimensions, ScrollView, Animated, Easing } from 'react-native';
import PropTypes from 'prop-types'

import TopicsItem from './topics-item'
import Burger from 'appteszt/js/components/burger'
import Search from 'appteszt/js/components/search'
import VideoItem from './video-item'
import HrLine from 'appteszt/js/components/hr-line'
import ThreePics from './three-pics'

import { scale, verticalScale } from 'appteszt/js/scaling';

import styles from './styles'

class Main extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            topics: [
                {
                    key: 1,
                    text: 'Latest',
                    active: true
                },
                {
                    key: 2,
                    text: 'Opinion'
                },
                {
                    key: 3,
                    text: 'Tech'
                },
                {
                    key: 4,
                    text: 'Sports'
                },
                {
                    key: 5,
                    text: 'Money'
                },
                {
                    key: 6,
                    text: 'Fake News'
                }
            ]
        };

        this.onDimensionChange = this.onDimensionChange.bind(this);
        //Dimensions.addEventListener('change', this.onDimensionChange);
    }

    onDimensionChange() {
        // To trigger redraw
        this.setState(Object.assign({}, this.state, {}));
    }

    componentWillUnmount() {
        //Dimensions.removeEventListener('change', this.onDimensionChange);
    }

    render() {
        var { height, width } = Dimensions.get('window');

        const topics = this.state.topics.map((item, i) => {
            return (
                <TopicsItem {...item}></TopicsItem>
            )
        });

        const videoItem = {
            text: 'Comment on the importance of human life',
            type: 'Video',
            commentCount: 45,
            image: require('appteszt/img/sidebar/img4.png')
        };

        const videoItem2 = {
            text: 'Comment on the importance of human life',
            type: 'Video',
            commentCount: 68,
            image: require('appteszt/img/sidebar/img3.png')
        };
        
        const threePicsItem = {
            text: 'Sensible Beauty Tips For Enhancing Your Appearance',
            topic: 'Fashion',
            commentCount: 221,
            image1: require('appteszt/img/sidebar/img5.png'),
            image2: require('appteszt/img/sidebar/img6.png'),
            image3: require('appteszt/img/sidebar/img5.png')
        };

        return (
            <View style={{ flex: 1 }}>
                <View style={{
                        flexDirection: 'row', justifyContent: 'space-between',
                        alignItems: 'center',
                        height: verticalScale(70)
                    }}>
                    <View>
                        <TouchableNativeFeedback onPress={() => this.props.showSide()}>
                            <View style={{ flex: 1 }}>
                                <Burger color='#f9596d'></Burger>
                            </View>
                        </TouchableNativeFeedback>
                    </View>
                    <View>
                        <Text style={{ fontSize: 22, fontWeight: 'bold', color: 'black' }}>News</Text>
                    </View>
                    <View style={{ transform: [
                        {
                            scale: 0.5
                        }
                    ] }}>
                        <Search color='#f9596d'></Search>
                    </View>
                </View>

                <View style={{
                        height: verticalScale(70), backgroundColor: 'white',
                        marginBottom: 5, marginTop: 5
                    }}>
                    <ScrollView horizontal={true} style={{ flex: 1, flexDirection: 'row', paddingBottom: 5 }}>
                        {topics}
                    </ScrollView>
                </View>

                <View style={{ backgroundColor: 'white', flex: 8 }}>
                    <ScrollView style={{ flex: 2 }}>
                        <ScrollView horizontal={true} style={{ height: verticalScale(160) }}>
                            <View style={styles.fRowImgView}>
                                <Image style={styles.image} source={require('appteszt/img/sidebar/img1.png')}></Image>
                            </View>
                            <View style={styles.fRowImgView}>
                                <Image style={styles.image} source={require('appteszt/img/sidebar/img2.png')}></Image>
                            </View>
                        </ScrollView>
                        
                        <HrLine></HrLine>
                        
                        <VideoItem changeView={this.props.changeView} {...videoItem}></VideoItem>

                        <HrLine></HrLine>

                        <ThreePics changeView={this.props.changeView} {...threePicsItem}></ThreePics>

                        {/* <HrLine /> */}

                        {/* <VideoItem changeView={this.props.changeView} {...videoItem2}></VideoItem> */}
                        {/* <View style={{ height: 140 }}>
                            <Image style={styles.image} source={require('appteszt/img/sidebar/img3.png')}></Image>
                        </View> */}
                    </ScrollView>
                </View>

                <View style={{ height: verticalScale(100) }}>
                    <HrLine></HrLine>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-around', marginTop: 8 }}>
                        <View style={{ }}>
                            <Image source={require('appteszt/img/sidebar/ico_book.png')}></Image>
                            <Text style={{ fontSize: 12, color: '#f9596d' }}>HOME</Text>
                        </View>
                        <View style={{ marginLeft: 2 }}>
                            <Image source={require('appteszt/img/sidebar/ico_video.png')}></Image>
                            <Text style={{ fontSize: 12, marginTop: 5 }}>VIDEO</Text>
                        </View>
                        <View style={{}}>
                            <Image style={{ marginLeft: 12 }} source={require('appteszt/img/sidebar/ico_discover.png')}></Image>
                            <Text style={{ fontSize: 12, marginTop: 0 }}>DISCOVER</Text>
                        </View>
                        <View style={{}}>
                            <Image style={{ marginLeft: 6 }} source={require('appteszt/img/sidebar/ico_profile.png')}></Image>
                            <Text style={{ fontSize: 12, marginTop: 0 }}>PROFILE</Text>
                        </View>
                    </View>
                </View>
            </View>)
    }
}

//CategorySelector.propTypes = {
//};

export default Main;
