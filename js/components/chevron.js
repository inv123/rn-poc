import React from 'react';
import { View, StyleSheet, Text, Button, TouchableWithoutFeedback, TouchableNativeFeedback, TextInput, Dimensions, ScrollView, Animated, Easing } from 'react-native';
import PropTypes from 'prop-types'

class Chevron extends React.Component {
    constructor(props) {
        super(props);

        this.state = {

        };

        this.onDimensionChange = this.onDimensionChange.bind(this);
        //Dimensions.addEventListener('change', this.onDimensionChange);
    }

    onDimensionChange() {
        // To trigger redraw
        //this.setState(Object.assign({}, this.state, {}));
    }

    componentWillUnmount() {
        //Dimensions.removeEventListener('change', this.onDimensionChange);
    }

    render() {
        var { height, width } = Dimensions.get('window');

        return (
            <View style={{ paddingLeft: 2 }}>
                <View style={{
                    backgroundColor: this.props.color,
                    height: 10,
                    width: 2,
                    transform: [
                        {
                            rotate: '-45deg'
                        },
                        {
                            translateY: 3
                        }
                    ]
                }}>
                </View>
                <View style={{
                    backgroundColor: this.props.color,
                    height: 10,
                    width: 2,
                    transform: [
                        {
                            rotate: '45deg'
                        },
                        {
                            translateY: -3
                        }
                    ]
                }}>
                </View>
            </View>)
    }
}

Chevron.propTypes = {
    color: PropTypes.string
};

export default Chevron;
