import React from 'react';
import { View, Text, Button, requireNativeComponent } from 'react-native';

var DropShadowView = requireNativeComponent('ShadowDroppView', DropShadowView);

export default DropShadowView;