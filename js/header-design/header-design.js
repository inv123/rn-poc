import React from 'react';
import { TouchableWithoutFeedback, Easing, Animated, View, Text, Button, Image, FlatList, TouchableOpacity, Dimensions, TouchableHighlight, StyleSheet } from 'react-native';

import styles from './styles'
import ListItem from './list-item'
import DropDownItem from './../plant-defaults/drop-down-item'
import DropShadowView from './../native/drop-shadow'
import Footer from './../plant-defaults/footer'

class HeaderDesign extends React.Component {
    static navigatorStyle = {
        drawUnderNavBar: true,
        navBarTranslucent: true,
        navBarHidden: true
    };

    constructor(props) {
        super(props);

        // Static stuff
        let array = [];
        array.push({ 
            key: 0,
            title: 'Furniture in the garden!',
            content: 'Spicy jalapeno bacon ipsum dolor amet biltong beef sirloin jowl kevin picanha shoulder flank meatloaf capicola cupim swine chicken corned beef.',
            image: require('./../../img/orangery/garden.png')
        });
        array.push({ 
            key: 1,
            title: 'Ponds, pools and other water bodies',
            content: 'Spicy jalapeno bacon ipsum dolor amet biltong beef sirloin jowl kevin picanha shoulder flank meatloaf capicola cupim swine chicken corned beef.',
            image: require('./../../img/orangery/water.png')
        });
        array.push({ 
            key: 2,
            title: 'DIY Greenhouses',
            content: 'Spicy jalapeno bacon ipsum dolor amet biltong beef sirloin jowl kevin picanha shoulder flank meatloaf capicola cupim swine chicken corned beef.',
            image: require('./../../img/orangery/greenhouse.png')
        });
        array.push({ 
            key: 3,
            title: 'DIY Greenhouses',
            content: 'Spicy jalapeno bacon ipsum dolor amet biltong beef sirloin jowl kevin picanha shoulder flank meatloaf capicola cupim swine chicken corned beef.',
            image: require('./../../img/orangery/water.png')
        });
        array.push({ 
            key: 4,
            title: 'DIY Greenhouses',
            content: 'Spicy jalapeno bacon ipsum dolor amet biltong beef sirloin jowl kevin picanha shoulder flank meatloaf capicola cupim swine chicken corned beef.',
            image: require('./../../img/orangery/water.png')
        });

        let dropDownOptions = [];
        dropDownOptions.push({ key: 0, title: 'Catalogue', icon: require('./../../img/orangery/tools.png') });
        dropDownOptions.push({ key: 1, title: 'Inspirations', icon: require('./../../img/orangery/recipes.png') });
        dropDownOptions.push({ key: 2, title: 'Tips', icon: require('./../../img/orangery/tools.png') });
        dropDownOptions.push({ key: 3, title: 'Recipes', icon: require('./../../img/orangery/recipes.png') });

        this.state = {
            array: array,
            dropDownOptions: dropDownOptions,
            orientation: false,
            dropDownVisible: false,

            droppDownPos: new Animated.Value(0),
            droppDownOppacity: new Animated.Value(0)
        }

        this.onChange = this.onChange.bind(this);
        Dimensions.addEventListener('change', this.onChange);
    }

    onChange() {
        this.setState(Object.assign({}, this.state, {
            orientation: !this.state.orientation
        }));
    }

    navBack() {
        this.props.navigator.pop({
            animated: true, // does the pop have transition animation or does it happen immediately (optional)
            animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the pop have different transition animation (optional)
        });
    }

    componentWillUnmount() {
        //chngHandler();
        Dimensions.removeEventListener('change', this.onChange)
    }

    _renderItem = ({item}) => {
        return (<ListItem {...item} />);
    }

    onDropDownClick() {
        if (this.state.dropDownVisible) {
            Animated.parallel([
                Animated.timing(this.state.droppDownPos, {
                    toValue: 0,
                    //easing: Easing.back(),
                    duration: 200,
                }),
                Animated.timing(this.state.droppDownOppacity, {
                    toValue: 0,
                    //easing: Easing.back(),
                    duration: 200,
                })
            ]).start();
        } else {
            Animated.parallel([
                Animated.timing(this.state.droppDownPos, {
                    toValue: 200,
                    //easing: Easing.back(),
                    duration: 300,
                }),
                Animated.timing(this.state.droppDownOppacity, {
                    toValue: 1,
                    //easing: Easing.back(),
                    duration: 300,
                })
            ]).start();
        }
        
        var newState = Object.assign({}, this.state, {
            dropDownVisible: !this.state.dropDownVisible,
        });

        this.setState(newState);
    }

    render() {
        var { height, width } = Dimensions.get('window');
        var dropDownOptions = this.state.dropDownOptions.map(function(object, i) {
            return <DropDownItem {...object}></DropDownItem>;
        });

        //var { droppDownPos, droppDownOppacity } = this.state.droppDownPos;
        return (
            <View style={{ flex: 1, flexDirection: 'column', backgroundColor: '#FEFEFE' }}>
                <View style={styles.header}>
                    <View style={{
                            flex: 1,
                            flexDirection: 'row',
                            justifyContent: 'space-between'
                        }}>
                        <TouchableHighlight onPress={()=> this.onDropDownClick() }>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <Image style={styles.topLeft} source={require('./../../img/orangery/topleft.png')}/>
                                <Image style={styles.downChevron} source={require('./../../img/down-chevron.png')}/>
                            </View>
                        </TouchableHighlight>
                        <Text style={styles.title}>The Big Book of Plants</Text>
                        <Image style={styles.search} source={require('./../../img/orangery/topright.png')}/>
                    </View>
                </View>
                
                <FlatList
                    data={this.state.array}
                    renderItem={this._renderItem}
                    extraData={this.state}
                />

                { this.state.dropDownVisible ? 
                    <DropShadowView style={styles.droppShadow} />
                : null }

                <Animated.View style={StyleSheet.flatten([styles.droppDown, { height: this.state.droppDownPos }])}>
                    {dropDownOptions}
                </Animated.View>

                <Footer navBack={() => this.navBack()} />
            </View>
        );
    }
}



export default HeaderDesign;