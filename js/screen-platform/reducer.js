import { combineReducers } from 'redux'
import * as actions from './actions'

const initialValue = {
    plants: {
        isFetching: false,
        receivedAt: Date.now(),
        items: []
    }
}

function plants(state = initialValue.plants, action) {
    switch(action.type) {
        case actions.FETCHPLANTS:
        return Object.assign({}, state, { isFetching: action.isFetching });
        break;
        case actions.RECEIVEDPLANTS:
        return Object.assign({}, state, { items: action.items, receivedAt: action.receivedAt, isFetching: false });
        break;
        default:
        return state;
    }
}

export default plants;
