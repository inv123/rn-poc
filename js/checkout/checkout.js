import React from 'react';
import { View, StyleSheet, Text, Button, TextInput, Dimensions, ScrollView, Animated, Easing, Platform } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';

import ThreeStageBackground from '../components/three-stage-background/three-stage-background'
import Cart from './cart/cart'
import OptionList from './option-list/option-list'
import CategorySelector from './category-selector/category-selector'
import CheckoutHeader from './header/checkout-header'
import { verticalScale, moderateScale } from '../scaling';

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from 'appteszt/redux/checkout/data/action'

// TODO align everything right on dimension change

class Checkout extends React.Component {
	static navigatorStyle = {
		drawUnderNavBar: true,
		navBarTranslucent: true,
		navBarHidden: true
	};

	constructor(props) {
		super(props);

		this.state = {
			stage: 0,
			...this.defaultValues()
		};

		const { dispatch } = props;
		this.boundActionCreators = bindActionCreators(actions, dispatch)

		this.onDimensionChange = this.onDimensionChange.bind(this);
		Dimensions.addEventListener('change', this.onDimensionChange);
	}

	defaultValues = () => {
		const { width } = Dimensions.get('window');

		return {
			catTransX: new Animated.Value(0),
			optTransX: new Animated.Value(width),
			cartTransX: new Animated.Value(width),

			// Tells the header when to begin animate
			headerState: 0,
		}
	}

	onDimensionChange() {
		this.setState(Object.assign({}, this.state, {
			...this.defaultValues()
		}));

		if (this.child) {
			this.child.resetStage();
			this.setState(Object.assign({}, this.state, {
				stage: 0,
				headerState: 0
			}));
		}
	}

	componentWillUnmount() {
		Dimensions.removeEventListener('change', this.onDimensionChange);
	}

	componentDidMount() {
		//alert(JSON.stringify(this.props.data));
		// let { dispatch } = this.props;
		// dispatch(actions.fetchData())
		//.then(() => alert(JSON.stringify(this.props.data)));

		this.setState(Object.assign({}, this.state, {
			stage: 0,
			headerState: 0
		}));
	}

	animateLeft(animIn, animOut) {
		var { height, width } = Dimensions.get('window');
		Animated.parallel([
			// Beuszó nézet
			Animated.timing(animIn, {
				toValue: 0,
				easing: Easing.out(Easing.exp),
				duration: 500,
			}),
			// Eltűnő nézet
			Animated.timing(animOut, {
				toValue: width,
				easing: Easing.out(Easing.exp),
				duration: 800,
			})
		]).start();
	}

	animateRight(animIn, animOut) {
		var { width } = Dimensions.get('window');
		Animated.parallel([
			// Beuszó nézet
			Animated.timing(animIn, {
				toValue: 0,
				easing: Easing.out(Easing.exp),
				duration: 500,
			}),
			// Eltűnő nézet
			Animated.timing(animOut, {
				toValue: width * -1,
				easing: Easing.out(Easing.exp),
				duration: 800,
			})
		]).start();
	}

	animationTimeSpan = 400;
	inProgress = false;
	onNextSlide() {
		// Forces every animation to be finished
		var incrStage = () => {
			// swap background
			this.child.next();

			this.setState(Object.assign({}, this.state, {
				headerState: this.state.headerState + 1
			}));

			this.inProgress = true;
			setTimeout(() => {
				this.setState(Object.assign({}, this.state, { stage: this.state.stage + 1 }));
				this.inProgress = false;
			}, this.animationTimeSpan);
		};
		if (this.inProgress == true) return;

		switch (this.state.stage) {
			case 0:
				this.animateRight(this.state.optTransX, this.state.catTransX);
				incrStage();
				break;
			case 1:
				this.animateRight(this.state.cartTransX, this.state.optTransX);
				incrStage();
				break;
			case 2:
			case 3:
			default:
				console.warn('Out of bounds!');
				break;
		}
	}

	onPrevSlide() {
		var decrStage = () => {
			// swap background
			this.child.prev();

			this.setState(Object.assign({}, this.state, {
				headerState: this.state.headerState - 1
			}));

			this.inProgress = true;
			setTimeout(() => {
				this.setState(Object.assign(this.state, this.state, { stage: this.state.stage - 1 }));
				this.inProgress = false;
			}, this.animationTimeSpan);
		};
		if (this.inProgress == true) return;

		switch (this.state.stage) {
			case 1:
				this.animateLeft(this.state.catTransX, this.state.optTransX);
				decrStage();
				break;
			case 2:
				this.animateLeft(this.state.optTransX, this.state.cartTransX);
				decrStage();
				break;
			case 3:
			case 0:
			default:
				console.warn('Out of bounds!');
				break;
		}
	}

	_requestStateChange(direction, item) {
		if (direction == 'next') {
			this.onNextSlide();
		} else {
			this.onPrevSlide();
		}
	}

	render() {
		const views = [
			CategorySelector,
			OptionList,
			Cart
		];

		return (
			<View style={{ flex: 1, flexDirection: 'column' }}>
				<ThreeStageBackground
					views={views}
					childrenProps={{
						dispatch: this.props.dispatch,
						data: this.props.data,
						requestSwitch: this._requestStateChange.bind(this),
						cart: this.props.cart
					}}
					ref={instance => { this.child = instance; }}
					backgroundColor='#f53b50'
				>
					<View style={{
						position: 'absolute',
						width: width,
						height: headerHeight,
					}} ref={(ins) => this.headerView = ins}>
						<CheckoutHeader
							navigator={this.props.navigator}
							counter={this.props.cart.count}
							headerStage={this.state.headerState}
							requestSwitch={this._requestStateChange.bind(this)}></CheckoutHeader>
					</View>
				</ThreeStageBackground>

				{/* <Button title="Next" onPress={()=>this.onNextSlide()}></Button>
                <Button title="Prev" onPress={()=>this.onPrevSlide()}></Button> */}
			</View>
		)
	}
}

export default connect(state => ({ data: state.data, cart: state.cart }))(Checkout)
