package com.appteszt;

import android.widget.TextView;

import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;

/**
 * Created by GergoN on 2017.12.06..
 */

public class CustomAkarmiViewmanager extends SimpleViewManager<TextView> {
    @Override
    public String getName() {
        return "MyBasicView";
    }

    /*@Override
    protected View createViewInstance(ThemedReactContext reactContext) {
        return new View(reactContext);
    }*/

    @Override
    protected TextView createViewInstance(ThemedReactContext reactContext) {
        TextView tv = new TextView(reactContext);
        tv.setText("hello from android !");
        return tv;
    }
}
