import React from 'react';
import { View, StyleSheet, Text, Button, Image, FlatList, Platform, TouchableWithoutFeedback, TouchableNativeFeedback, TextInput, Dimensions, ScrollView, Animated, Easing } from 'react-native';
import PropTypes from 'prop-types'

const STAGE1 = 1;
const STAGE2 = 2;
const STAGE3 = 3;

const CURRENTSTAGE = 0;

class StagedBackground extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            ...this.defaultValues()
        };

        this.onDimensionChange = this.onDimensionChange.bind(this);
        Dimensions.addEventListener('change', this.onDimensionChange);
    }

    defaultValues() {
        var { height, width } = Dimensions.get('window');

        const startingTransX = -2 * (width / 3);
        //alert(startingTransX)
        this.currentPadding = startingTransX;

        return {
            first: new Animated.Value(startingTransX)
        };
    }

    onDimensionChange() {
        // To trigger redraw
        this.setState(Object.assign({}, this.state, {}));
    }

    componentWillUnmount() {
        Dimensions.removeEventListener('change', this.onDimensionChange);
    }

    currentPadding = 0
    stageOn(animateParam) {
        var { height, width } = Dimensions.get('window');

        const paddingAmount = (width / 3);
        this.currentPadding = this.currentPadding + paddingAmount;

        Animated.timing(animateParam, {
            toValue: this.currentPadding,
            easing: Easing.out(Easing.exp),
            duration: 800,
        }).start()
    }

    stageOff(animateParam) {
        var { height, width } = Dimensions.get('window');

        const paddingAmount = (width / 3);
        this.currentPadding = this.currentPadding - paddingAmount;

        Animated.timing(animateParam, {
            toValue: this.currentPadding,
            easing: Easing.out(Easing.exp),
            duration: 800,
        }).start();
    }

    prev() {
        if (CURRENTSTAGE - 1 < 0) {
            console.warn('No more stages! (prev)');
            return 0;
        } else {
            CURRENTSTAGE--;
        }
        this.stageOff(this.state.first);
    }

    next() {
        if (CURRENTSTAGE + 1 >= STAGE3) {
            console.warn('No more stages! Max stage is 3! (next)');
            return 0;
        } else {
            CURRENTSTAGE++;
        }
        this.stageOn(this.state.first);
    }

    onPrevPressed() {
        this.prev();
    }
    onNextPressed() {
        this.next();
    }

    resetStage() {
        this.setState(Object.assign({}, this.state, this.defaultValues()));
        CURRENTSTAGE = 0;
    }

    componentDidMount() {
        this.resetStage();

        if (this.props.startingStage) {
            if (this.props.startingStage == 1) {
                setTimeout(() => {
                    this.next();
                }, 0);
            }
        }
    }

    render() {
        var { height, width } = Dimensions.get('window');

        return (
            <View style={{ flex: 1, flexDirection: 'row' }}>
                <View style={{ flex: 1, zIndex: 1 }}>
                    <Animated.View style={{
                        flex: 1,
                        backgroundColor: '#f53b50',
                        transform: [
                            {
                                translateX: this.state.first
                            }
                        ],
                    }}>
                    </Animated.View>
                </View>
                
                <View style={{ 
                    position: 'absolute',
                    width: width,
                    height: height,
                    zIndex: 4,
                    ...Platform.select({
                        ios: {
                            marginTop: 30
                        }
                    })
                }}>
                    {this.props.children}
                    {/* <Button title='Animate' onPress={() => this.onNextPressed()}></Button>
                    <Button onPress={()=> this.onPrevPressed()} title="Prev"></Button> */}
                </View>
            </View>)
    }
}

//CategorySelector.propTypes = {
//};

export default StagedBackground;
