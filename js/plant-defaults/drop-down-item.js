import React from 'react';
import { View, Text, Button, Image, FlatList, TouchableOpacity, Dimensions } from 'react-native';
import { scale, moderateScale, verticalScale } from './../scaling';

class DropDownItem extends React.Component {
    _onPressButton () {
        //alert(123)
    }

    render() {
        var { height, width } = Dimensions.get('window');
        return (
            // TODO TouchableOpacity
            // <TouchableOpacity onPress={this._onPressButton}>
                <View style={{ flex: 1, flexDirection: 'row', margin: 5, borderBottomWidth: 1, borderBottomColor: '#D7E1EA' }}>
                    <Image style={{ 
                        width: scale(40),
                        height: verticalScale(50),
                        transform: [
                            { scaleY: 1 },
                            { scaleX: 0.5 }
                        ] 
                    }} source={this.props.icon}/>
                    <Text style={{ fontFamily: 'cronusRound', fontSize: 16 }}>{this.props.title}</Text>
                </View>)
                {/* <View style={{ flex: 1, flexDirection: 'column' }}>
                    <View>
                        <Image style={{  }} source={this.props.icon}/>
                        <Text style={{ fontFamily: 'cronusRound', fontSize: 16, borderBottomWidth: 1, borderBottomColor: '#D7E1EA'  }}>{this.props.title}</Text>
                    </View>
                </View>
            </TouchableOpacity> */}
            
    }
}

export default DropDownItem;