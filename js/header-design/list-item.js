import React from 'react';
import { View, Text, Button, Image, FlatList, TouchableOpacity, Dimensions, StyleSheet } from 'react-native';

import styles from './styles'

class ListItem extends React.Component {
    _onPressButton () {
        //alert(123)
    }

    render() {
        var { height, width } = Dimensions.get('window');
        return (
            <TouchableOpacity onPress={this._onPressButton}>
                <View style={{ margin: 10, marginTop:0, marginLeft: 15, borderBottomWidth: 1, borderBottomColor: '#D7E1EA', paddingBottom: 0 }}>
                    <View style={{ flex: 1 }}>
                        <Text style={styles.itemHeader}>{this.props.title}</Text>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', }}>
                            <Image style={styles.itemImage} source={this.props.image}/>
                            <Text style={StyleSheet.flatten([styles.itemContent, { width: width - 140 }])}>{this.props.content}</Text>
                        </View>
                        <View style={{ flex:1, flexDirection: 'row', justifyContent: 'flex-end' }}>
                            <Image style={{
                                transform: [
                                    { rotate: '-90deg'},
                                    { scaleY: 0.5 },
                                    { scaleX: 0.5 }
                                ],
                            }} source={require('./../../img/down-chevron.png')}/>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
            )
    }
}

export default ListItem;