import { Navigation } from 'react-native-navigation';

import Main from './js/main/main';
import SeriesList from './js/series-list/series-list';
import ScreenPlatform from './js/screen-platform/screen-platform'
import HeaderDesign from './js/header-design/header-design'
import PlantList from './js/plant-list/plant-list'
import PlantDefaults from './js/plant-defaults/index'
import PlantDetails from './js/plant-details/plant-details'
import Checkout from './js/checkout/checkout'
import BackgroundChild from './js/background-withchildren/background-child'
import Drawer from './js/checkout/drawer'
import Login from 'appteszt/js/checkout/login/login'
import SideBarTrans from 'appteszt/js/sidebar-trans/sidebar-trans'
import NavPoc from 'appteszt/js/nav-poc/nav-poc'
import NavDetails from 'appteszt/js/nav-poc/nav-details'

// register all screens of the app (including internal ones)
export function registerScreens(store, provider) {
  Navigation.registerComponent('main.index', () => Main, store, provider);
  Navigation.registerComponent('serieslist.index', () => SeriesList, store, provider);
  Navigation.registerComponent('screenPlatform.index', () => ScreenPlatform, store, provider);
  Navigation.registerComponent('headerDesign.index', () => HeaderDesign);
  Navigation.registerComponent('plants.list', () => PlantList);
  Navigation.registerComponent('plants.default', () => PlantDefaults);
  Navigation.registerComponent('plants.details', () => PlantDetails);
  Navigation.registerComponent('example.checkout', () => Checkout, store, provider);
  Navigation.registerComponent('example.backgroundChild', () => BackgroundChild);
  Navigation.registerComponent('example.login', () => Login, store, provider);
  Navigation.registerComponent('example.sidebarTrans', () => SideBarTrans, store, provider);

  Navigation.registerComponent('navpoc.main', () => NavPoc);
  Navigation.registerComponent('navpoc.details', () => NavDetails);

  Navigation.registerComponent('example.drawer', () => Drawer);
}