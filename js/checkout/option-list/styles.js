import { StyleSheet, Dimensions } from 'react-native'
import { scale, moderateScale, verticalScale } from './../../scaling';

var { height, width } = Dimensions.get('window');

const styles = {
    categorySelectorItem: {
        margin: 5,
        marginLeft: 0,
        //padding: 0,
        paddingTop: 15,
        paddingBottom: 15,

        marginBottom: 15,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        
        alignSelf: 'center',
        backgroundColor: 'white',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',

        elevation: 2
        // TODO ios shadows
    },
    itemText: {
        fontSize: scale(25)
    },
    itemSubText: {
        fontSize: scale(14)
    }

}

export default StyleSheet.create(styles);